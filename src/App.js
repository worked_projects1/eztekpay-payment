import React, { lazy } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import MUITheme from './muiTheme';
import routes from './utils/routes';
import Spinner from './components/Spinner';

const OtpPage = lazy(() => import('./pages/OtpPage'));
const AppPage = lazy(() => import('./pages/AppPage'));
const PayFormPage = lazy(() => import('./pages/PayFormPage'));
const SuccessPage = lazy(() => import('./pages/SuccessPage'));
const NotFound = lazy(() => import('./pages/NotFound'));

function App() {
  return (
    <MUITheme>
      <Router>
        <Routes>
          <Route path={"/"} element={<React.Suspense fallback={<Spinner showHeight />}>
            <AppPage />
          </React.Suspense>}
          >
            <Route index path={routes.OTP_FORM} element={
              <React.Suspense fallback={<Spinner showHeight />}>
                <OtpPage />
              </React.Suspense>} />

            <Route exact path={routes.PAYMENT} element={
              <React.Suspense fallback={<Spinner showHeight />}>
                <PayFormPage />
              </React.Suspense>} />

            <Route exact path={routes.SUCCESS} element={
              <React.Suspense fallback={<Spinner showHeight />}>
                <SuccessPage />
              </React.Suspense>} />

            <Route path="*" element={<React.Suspense fallback={<Spinner showHeight />}>
              <NotFound />
            </React.Suspense>} />
          </Route>
        </Routes>
      </Router>
    </MUITheme>
  );
}

export default App;
