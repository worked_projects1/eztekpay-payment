/*
 *
 * Session actions
 *
 */

import {
    VERIFY_OTP,
    VERIFY_OTP_SUCCESS,
    VERIFY_OTP_ERROR,
    GET_OTP,
    CLEAR_SESSION_ERRORS,
    CREATE_CUSTOM_ERRORS,
    CLEAR_SESSION,
    LINK_OPENED,
    LINK_OPENED_SUCCESS,
    LINK_OPENED_ERROR
} from './constants';
import { createAction } from '@reduxjs/toolkit';

export const verifyOtp = createAction(VERIFY_OTP);
export const verifyOtpSuccess = createAction(VERIFY_OTP_SUCCESS);
export const verifyOtpError = createAction(VERIFY_OTP_ERROR);
export const getOtp = createAction(GET_OTP);
export const clearSessionErrors = createAction(CLEAR_SESSION_ERRORS);
export const createCustomErrors = createAction(CREATE_CUSTOM_ERRORS);
export const clearSession = createAction(CLEAR_SESSION);
export const linkOpened = createAction(LINK_OPENED);
export const linkOpenedSuccess = createAction(LINK_OPENED_SUCCESS);
export const linkOpenedError = createAction(LINK_OPENED_ERROR);

export default {
    verifyOtp,
    verifyOtpSuccess,
    verifyOtpError,
    getOtp,
    clearSessionErrors,
    createCustomErrors,
    clearSession,
    linkOpened,
    linkOpenedSuccess,
    linkOpenedError
}