

export const OTP_VERIFY_ERROR = 'Failed to verify OTP';
export const DEFAULT_LINK_OPENED_ERROR = 'Failed to open the link';