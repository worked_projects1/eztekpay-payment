import {
    // VERIFY_OTP,
    // GET_OTP,
    VERIFY_OTP_SUCCESS,
    VERIFY_OTP_ERROR,
    CLEAR_SESSION_ERRORS,
    CREATE_CUSTOM_ERRORS,
    CLEAR_SESSION,
    // LINK_OPENED,
    LINK_OPENED_SUCCESS,
    LINK_OPENED_ERROR
} from './constants';
import { createReducer } from '@reduxjs/toolkit';

const initialState = {
    userOtp: false, error: {}, success: {}, activeSession: 1, secret: false, version: '1.0', loading: false, appVersion: false,
    hospital: {},
    userData: {}
    // userData: {
    //     "message": "otp verified successfully",
    //     "invoice_amount": 54.45,
    //     "invoice_number": "SEL6660450",
    //     "patient_name": "Test Patient",
    //     "hospital_name": "Test Hospital1"
    // }
};

/**
 * @param {object} state 
 * @param {object} action 
 */

const appreducers = createReducer(initialState, (builder) => {
    builder
        .addCase(VERIFY_OTP_SUCCESS, (state, action) => {
            state.userOtp = true;
            state.userData = action.payload.userData;
            state.error = {};
            state.success = {};
            state.loading = false;
        })
        .addCase(VERIFY_OTP_ERROR, (state, action) => {
            state.error = action.payload.error;
            state.success = {};
            state.loading = false;
        })
        .addCase(CLEAR_SESSION_ERRORS, (state, action) => {
            state.error = {}
        })
        .addCase(CREATE_CUSTOM_ERRORS, (state, action) => {
            state.error = action.payload.error;
            state.success = {};
            state.loading = false;
        })
        .addCase(CLEAR_SESSION, (state, action) => {
            state.userOtp = false;
            state.userData = {};
            state.error = {};
            state.success = {};
            state.loading = false;
        })
        .addCase(LINK_OPENED_SUCCESS, (state, action) => {
            state.hospital = action.payload.success
        })
        .addCase(LINK_OPENED_ERROR, (state, action) => {
            state.error = action.payload.error;
            state.success = {};
            state.loading = false;
        })
});

export default appreducers;
