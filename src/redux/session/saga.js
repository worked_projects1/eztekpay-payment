import {
    put, call, all, take
} from 'redux-saga/effects';
// import { setAuthToken } from '../../utils/api';

import { OTP_VERIFY_ERROR, DEFAULT_LINK_OPENED_ERROR } from './errorConstants';

import { verifyOtpApi, linkOpenedApi } from './remotes';

import {
    // verifyOtp,
    verifyOtpSuccess,
    verifyOtpError,
    // getOtp,
    // linkOpened,
    linkOpenedSuccess,
    linkOpenedError
} from './actions';

import {
    VERIFY_OTP,
    // VERIFY_OTP_SUCCESS,
    // VERIFY_OTP_ERROR,
    // GET_OTP,
    LINK_OPENED,
    // LINK_OPENED_SUCCESS,
    // LINK_OPENED_ERROR
} from './constants';


export function* varifyOtpSaga() {
    while (true) {
        // eslint-disable-line no-constant-condition
        const action = yield take(VERIFY_OTP);
        const { payload, nextStep, loader } = action.payload;

        if (action) {
            try {
                let result = yield call(verifyOtpApi, payload);
                if (result) {
                    yield put(verifyOtpSuccess(Object.assign({}, { userData: result })));
                    if (nextStep) {
                        nextStep();
                    }
                }
            } catch (error) {
                const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : OTP_VERIFY_ERROR;
                yield put(verifyOtpError({ error: Err }));
            } finally {
                if (loader) {
                    loader();
                }
            }
        }
    }
}

export function* linkOpenedSaga() {
    while (true) {
        // eslint-disable-line no-constant-condition
        const action = yield take(LINK_OPENED);
        const { payload, loader } = action.payload;
        if (action) {
            try {
                let result = yield call(linkOpenedApi, payload);
                if (result) {
                    yield put(linkOpenedSuccess(Object.assign({}, { success: result })));
                }
            } catch (error) {
                const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_LINK_OPENED_ERROR;
                yield put(linkOpenedError({ error: Err }));
            } finally {
                if (loader) {
                    loader();
                }
            }
        }
    }
}

export function* sessionSagas() {
    yield all([
        varifyOtpSaga(),
        linkOpenedSaga()
    ]);
}

export default sessionSagas;