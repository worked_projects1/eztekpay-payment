

import api from '../../utils/api';

/**
 * @param {object} record 
 */
export function verifyOtpApi(record) {
    return api
        .post(`/rest/payments/verifyPaymentOtp`, record)
        .then(response => response.data)
        .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function linkOpenedApi(record) {
    return api
        .post(`/rest/patients/linkOpened`, record)
        .then(response => response.data)
        .catch(error => Promise.reject(error));
}
