
export const VERIFY_OTP = 'eztekpay/session/VERIFY_OTP';
export const GET_OTP = 'eztekpay/session/GET_OTP';
// export const LOG_IN_ERROR = 'lg/session/LOG_IN_ERROR';,
export const VERIFY_OTP_ERROR = 'eztekpay/session/VERIFY_OTP_ERROR';
export const VERIFY_OTP_SUCCESS = 'eztekpay/session/VERIFY_OTP_SUCCESS';
export const CLEAR_SESSION_ERRORS = 'eztekpay/session/CLEAR_SESSION_ERRORS';
export const CREATE_CUSTOM_ERRORS = 'eztekpay/session/CREATE_CUSTOM_ERRORS';
export const CLEAR_SESSION = 'eztekpay/session/CLEAR_SESSION';
export const LINK_OPENED = 'eztekpay/session/LINK_OPENED';
export const LINK_OPENED_SUCCESS = 'eztekpay/session/LINK_OPENED_SUCCESS';
export const LINK_OPENED_ERROR = 'eztekpay/session/LINK_OPENED_ERROR';