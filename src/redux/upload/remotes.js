/**
 * uploads - remotes
 */

import api from "../../utils/api";

/**
 * @param {string} file_name 
 * @param {string} content_type 
 */
export function getInvoiceRecieptUploadUrl(file_name, content_type) {
    return api.post(`/rest/payment/getUploadURL`, Object.assign({}, { file_name, content_type })).then((response) => response.data).catch((error) => Promise.reject(error));
}

const remotes = {
    getInvoiceRecieptUploadUrl,
}

export default remotes;