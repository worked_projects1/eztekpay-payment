import {
  all, fork
} from 'redux-saga/effects';
import sessionSagas from './session/saga';
import paymentSagas from './payment/saga';
import zipCodeSagas from './zipCodes/saga';

export default function* rootSaga() {
  yield all([
    fork(sessionSagas),
    fork(paymentSagas),
    fork(zipCodeSagas)
  ]);
}
