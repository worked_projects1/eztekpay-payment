

export const SETUP_STRIPE_PAYMENT = 'eztekpay/payment/SETUP_STRIPE_PAYMENT';
export const SETUP_STRIPE_PAYMENT_SUCCESS = 'eztekpay/payment/SETUP_STRIPE_PAYMENT_SUCCESS';
export const SETUP_STRIPE_PAYMENT_ERROR = 'eztekpay/payment/SETUP_STRIPE_PAYMENT_ERROR';

export const CLEAR_ERRORS = 'eztekpay/payment/CLEAR_ERRORS';
export const CREATE_CUSTOM_ERRORS = 'eztekpay/payment/CREATE_CUSTOM_ERRORS';