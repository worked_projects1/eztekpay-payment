

import api from '../../utils/api';

/**
 * @param {object} record 
 */
export function setupPaymentApi(record) {
    return api
        .post(`/payments/collectPayment`, record)
        .then(response => response.data)
        .catch(error => Promise.reject(error));
}
