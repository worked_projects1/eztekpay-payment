

import {
    SETUP_STRIPE_PAYMENT,
    SETUP_STRIPE_PAYMENT_SUCCESS,
    SETUP_STRIPE_PAYMENT_ERROR,
    CLEAR_ERRORS,
    CREATE_CUSTOM_ERRORS
} from './constants';
import { createAction } from '@reduxjs/toolkit';

export const setupStripePayment = createAction(SETUP_STRIPE_PAYMENT);
export const setupStripePaymentSuccess = createAction(SETUP_STRIPE_PAYMENT_SUCCESS);
export const setupStripePaymentError = createAction(SETUP_STRIPE_PAYMENT_ERROR);
export const clearErrors = createAction(CLEAR_ERRORS);
export const createCustomErrors = createAction(CREATE_CUSTOM_ERRORS);

export default {
    setupStripePayment,
    setupStripePaymentSuccess,
    setupStripePaymentError,
    clearErrors,
    createCustomErrors
}

