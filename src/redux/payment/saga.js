import {
    put, call, all, take
} from 'redux-saga/effects';
// import { setAuthToken } from '../../utils/api';

import { DEFAULT_STRIPE_PAYMENT_ERROR } from './errorConstants';

import { setupPaymentApi } from './remotes';

import {
    // setupStripePayment,
    setupStripePaymentSuccess,
    setupStripePaymentError
} from './actions';

import {
    SETUP_STRIPE_PAYMENT,
    // SETUP_STRIPE_PAYMENT_SUCCESS,
    // SETUP_STRIPE_PAYMENT_ERROR
} from './constants';


export function* setupStripePaymentSaga() {
    while (true) {
        // eslint-disable-line no-constant-condition
        const action = yield take(SETUP_STRIPE_PAYMENT);
        const { payload, nextStep, loader } = action.payload;

        if (action) {
            try {
                let result = yield call(setupPaymentApi, payload);
                if (result) {
                    yield put(setupStripePaymentSuccess(Object.assign({}, { success: result })));
                    if (nextStep) {
                        nextStep();
                    }
                }
            } catch (error) {
                const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_STRIPE_PAYMENT_ERROR;
                yield put(setupStripePaymentError({ error: Err }));
            } finally {
                if (loader) {
                    loader();
                }
            }
        }
    }
}

export function* paymentSagas() {
    yield all([
        setupStripePaymentSaga()
    ]);
}

export default paymentSagas;