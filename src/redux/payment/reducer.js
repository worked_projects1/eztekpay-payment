

import {
    // SETUP_STRIPE_PAYMENT,
    SETUP_STRIPE_PAYMENT_SUCCESS,
    SETUP_STRIPE_PAYMENT_ERROR,
    CLEAR_ERRORS,
    CREATE_CUSTOM_ERRORS
} from './constants';

import { createReducer } from '@reduxjs/toolkit';

const initialState = { clientsecret: false, error: {}, success: {} };

/**
 * @param {object} state 
 * @param {object} action 
 */

const paymentreducers = createReducer(initialState, (builder) => {
    builder
        .addCase(SETUP_STRIPE_PAYMENT_SUCCESS, (state, action) => {
            state.error = {};
            state.success = action.payload.success;
            state.loading = false;
        })
        .addCase(SETUP_STRIPE_PAYMENT_ERROR, (state, action) => {
            state.error = action.payload.error;
            state.success = {};
            state.loading = false;
        })
        .addCase(CLEAR_ERRORS, (state, action) => {
            state.error = {}
        })
        .addCase(CREATE_CUSTOM_ERRORS, (state, action) => {
            state.error = action.payload.error;
            state.success = {};
            state.loading = false;
        })
});

export default paymentreducers;