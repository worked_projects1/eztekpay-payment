

import appreducers from "./session/reducer";
import paymentreducers from "./payment/reducer";
import zipCodereducers from "./zipCodes/reducer";

const rootReducer = {
  session: appreducers,
  payment: paymentreducers,
  zipCodes: zipCodereducers
};

export default rootReducer;
