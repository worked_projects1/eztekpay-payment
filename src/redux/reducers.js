
import rootReducer from "./rootReducer";
import { combineReducers } from "@reduxjs/toolkit";
import { reducer as formReducer } from 'redux-form';


export function createReducer(injectedReducers = {}) {
    const basicReducer = combineReducers({
        ...injectedReducers,
        ...rootReducer,
        form: formReducer
        // other non-injected reducers can go here...
    });


    return basicReducer;
}