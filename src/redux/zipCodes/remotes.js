
import { staticApi } from '../../utils/api';

/**
 * @param {object} record 
 */
export function getZipcodesApi(record) {
    return staticApi
        .get()
        .then(response => response.data)
        .catch(error => Promise.reject(error));
}