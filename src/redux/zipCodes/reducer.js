import {
    GET_ZIPCODES_SUCCESS,
    GET_ZIPCODES_ERROR
} from './constants';

import { createReducer } from '@reduxjs/toolkit';

const initialState = { zipCodeData: new Map(), error: {}, success: {}, loading: false };

/**
 * @param {object} state 
 * @param {object} action 
 */

const zipCodereducers = createReducer(initialState, (builder) => {
    builder
        .addCase(GET_ZIPCODES_SUCCESS, (state, action) => {
            state.zipCodeData = action.payload.record;
            state.error = {};
            state.success = action.payload.success;
            state.loading = false;
        })
        .addCase(GET_ZIPCODES_ERROR, (state, action) => {
            state.error = action.payload.error;
            state.success = {};
            state.loading = false;
        })
});

export default zipCodereducers;