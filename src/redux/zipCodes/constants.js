

export const GET_ZIPCODES = 'eztekpay/payment/GET_ZIPCODES';
export const GET_ZIPCODES_SUCCESS = 'eztekpay/payment/GET_ZIPCODES_SUCCESS';
export const GET_ZIPCODES_ERROR = 'eztekpay/payment/GET_ZIPCODES_ERROR';
