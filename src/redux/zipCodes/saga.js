import {
    put, call, all, take
} from 'redux-saga/effects';
// import { setAuthToken } from '../../utils/api';

import { DEFAULT_ZIPCODES_GET_ERROR } from './errorConstants';

import { getZipcodesApi } from './remotes';

import {
    getZipCodeSuccess,
    getZipCodeError
} from './actions';

import {
    GET_ZIPCODES
} from './constants';


export function* getZipCodeSaga() {
    while (true) {
        // eslint-disable-line no-constant-condition
        const action = yield take(GET_ZIPCODES);

        if (action) {
            try {
                let result = yield call(getZipcodesApi);
                if (result) {
                    const zipcodeMap = new Map(result);
                    yield put(getZipCodeSuccess(Object.assign({}, { record: zipcodeMap })));
                }
            } catch (error) {
                const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_ZIPCODES_GET_ERROR;
                yield put(getZipCodeError({ error: Err }));
            }
        }
    }
}

export function* zipCodeSagas() {
    yield all([
        getZipCodeSaga()
    ]);
}

export default zipCodeSagas;