

import {
    GET_ZIPCODES,
    GET_ZIPCODES_SUCCESS,
    GET_ZIPCODES_ERROR
} from './constants';
import { createAction } from '@reduxjs/toolkit';

export const getZipCode = createAction(GET_ZIPCODES);
export const getZipCodeSuccess = createAction(GET_ZIPCODES_SUCCESS);
export const getZipCodeError = createAction(GET_ZIPCODES_ERROR);

export default {
    getZipCode,
    getZipCodeSuccess,
    getZipCodeError
}
