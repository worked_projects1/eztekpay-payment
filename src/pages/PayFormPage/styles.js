import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    formContainer: {
        border: "1px solid #F8F9FA",
        background: "#FFF",
        boxShadow: "0px 4px 20px 0px rgba(238, 238, 238, 0.50)",
        padding: '24px'
    },
    loadingBtn: {
        padding: theme.spacing(1.5, 2),
        fontSize: '14px',
        lineHeight: '1.5',
        fontFamily: 'Poppins-SemiBold',
        height: '50px',
        borderRadius: "50px",
        // backgroundColor: '#FFAA01',
        backgroundColor: '#385C9F',
        boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
        textTransform: 'capitalize',
        "& .MuiLoadingButton-loadingIndicator": {
            color: "#FFF"
        },
        "& .Mui-disabled": {
            // backgroundColor: '#FFAA01',
            backgroundColor: '#385C9F'
        }
    },

    root: {
        // height: '100vh',
        flexGrow: 1
    },
    component: {
        // padding: '4%',
        // [theme.breakpoints.down('md')]: {
        //     padding: '10% 7% 7% 7%',
        // },
        padding: '3% 16%',
        height: '100%',
        width: '100%',
        [theme.breakpoints.down('sm')]: {
            padding: '4%',
        },
        [theme.breakpoints.down('md')]: {
            padding: '4%',
        },
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    leftSideItem: {
        padding: '4%',
        [theme.breakpoints.down('md')]: {
            padding: '7%',
        },
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    FormPage: {
        display: 'flex',
        alignItems: 'flex-end',
        flexDirection: 'column',
        backgroundColor: 'grey',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundAttachment: 'fixed'
    },
    img: {
        width: '65%'
    }
}));

export default useStyles;