import React, { lazy, Suspense } from "react";
import { Grid, Typography, useMediaQuery } from '@mui/material';
import useStyles from "./styles";
import { useTheme } from "@mui/system";
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import CustomisedSnackBar from '../../components/CustomisedSnackBar';
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { clearErrors, setupStripePayment } from "../../redux/payment/actions";
import PaymentForm from "../../components/PaymentForm";
// import SuccessAnimation from "../../components/SuccessAnimation";
// import EzTekPAYLogo from '../../images/EzTekPAY.svg';
// import ProviderLogo from '../../images/provider.png';
import Spinner from "../../components/Spinner";
import ReactPDF from "@react-pdf/renderer";
import PdfCreator from "../../components/PdfCreator";
import { getInvoiceRecieptUploadUrl } from "../../redux/upload/remotes";
import { uploadFile } from "../../utils/api";
// import moment from "moment";

const SuccessAnimation = lazy(() => import('../../components/SuccessAnimation'));

const PayFormPage = () => {
    const [loadBtn, setLoadBtn] = React.useState(false);
    const [errorRes, setError] = React.useState(false);
    const [paymentSuccess, setPaymentSuccess] = React.useState(false);
    const { classes } = useStyles();
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.down('sm'));
    const lg = useMediaQuery(theme.breakpoints.up('lg'));
    const elements = useElements();
    const stripe = useStripe();
    const dispatch = useAppDispatch();
    //payment reducers
    const errors = useAppSelector(state => state?.payment?.error);
    // const success = useAppSelector(state => state?.payment.success);

    //session reducers
    const userData = useAppSelector(state => state?.session?.userData) || {};
    const invoice_details = userData?.invoice_details ?? [];
    const sum_of_pending_amounts = (invoice_details || []).reduce((sum, item) => {
        return sum + (item?.invoice_amount || 0);
    }, 0);
    const hospital_logo = userData?.hospital_details?.logo_file || ""

    React.useEffect(() => {
        setPaymentSuccess(false);
    }, [])

    const nextStep = () => {
        setPaymentSuccess(true);
    }

    const addressObj = (data) => {
        return Object.assign({}, { city: data.city, country: "US", line1: data.address, postal_code: data.postal_code, state: data.state });
    }
    const paymentFn = async (data, address, s3_file_key, public_url, payload) => {
        // const card = elements.getElement(CardElement);
        // const payload = await stripe.createPaymentMethod({
        //     type: 'card',
        //     card,
        //     billing_details: { name: data.name, address: address },
        // });
        // console.log("card = ", card, "payload = ", payload);
        // if (payload.error) {
        //     setLoadBtn(false);
        //     setError(payload.error?.message || 'Something went wrong. Please check address or card details');
        // } else {
        if (userData && payload?.paymentMethod) {
            const payloadToApi = Object.assign({}, {
                // "amount_to_collect": userData?.invoice_amount,
                "amount_to_collect": data?.paid_amount,
                // "sum_of_pending_amounts": Number(userData?.sum_of_pending_amounts || 0),
                "payment_description": "Payment with a card",
                "payment_method": payload.paymentMethod.id,
                "invoice_details": data?.amount_details,
                "patient_id": userData?.patient_id,
                "hospital_id": userData?.hospital_id,
                "s3_file_key": s3_file_key,
                "invoice_pdf": public_url
            });
            dispatch(setupStripePayment({ payload: payloadToApi, nextStep: nextStep, loader: () => setLoadBtn(false) }))
        } else {
            setLoadBtn(false);
            setError('Something went wrong');
        }
        // }
    }

    const uploadToBucket = ({ fileName, fileContentType, blob, data, payload }) => {

        const address = addressObj(data);
        const Upload = getInvoiceRecieptUploadUrl;
        if (blob) {
            Upload(fileName, fileContentType)
                .then(async (result) => {
                    const { upload_url: uploadURL, public_url, s3_file_key } = result;

                    if (uploadURL) {
                        await uploadFile(uploadURL, blob, fileContentType).then(async () => {
                            paymentFn(data, address, s3_file_key, public_url, payload);
                        })
                    } else {
                        setLoadBtn(false);
                        setError('Upload Failed');
                    }
                })
                .catch((err) => {
                    setLoadBtn(false);
                    setError('Upload Failed');
                });
        } else {
            paymentFn(data, address, "", "", payload);
        }
    }

    const handleSubmit = async (data) => {
        const address = addressObj(data);
        setLoadBtn(true);
        const card = elements.getElement(CardElement);
        const payload = await stripe.createPaymentMethod({
            type: 'card',
            card,
            billing_details: { name: data.name, address: address },
        });

        if (payload.error) {
            setLoadBtn(false);
            setError(payload.error?.message || 'Something went wrong. Please check address or card details');
        } else {
            const blob = userData?.receipts ? await ReactPDF.pdf(<PdfCreator
                userData={userData}
                patient_data={Object.assign({}, { name: data.name, address: address })}
                amount_details={data?.amount_details}
                cardData={payload}
            />).toBlob() : null;
            // const url = blob !== null ? URL.createObjectURL(blob) : null;

            // if (url) {
            //     window.open(url, '_blank');
            // }
            uploadToBucket({
                fileName: `Payment_Receipt_${Date.now()}.pdf`,
                fileContentType: "application/pdf",
                blob: blob,
                data: data,
                payload: payload
            });

            // setLoadBtn(false);
        }
    }

    return (paymentSuccess
        || sum_of_pending_amounts === 0
    ) ? <Suspense fallback={<Spinner showHeight />}>
        <Grid container sx={{
            height: '100%',
            alignItems: 'center',
            justifyContent: 'center'
        }}>
            <Grid sx={{
                textAlign: 'center'
            }}>
                <SuccessAnimation />
                <Typography sx={{
                    fontFamily: 'Poppins-SemiBold',
                    // color: "#2DCFC1",
                    color: '#385C9F',
                    fontSize: '26px '
                }}>
                    {sum_of_pending_amounts === 0 ? "Your payment was already processed." : "Your payment has been successfully processed."}
                </Typography>
            </Grid>
        </Grid>
    </Suspense> : <Grid container component="main" className={classes.root}>
        <Grid
            className={classes.component}
            id='right-container'
            sx={{
                backgroundColor: '#F8F8FB',
                flexDirection: 'column'
            }}>
            {/* {hospital_logo ? <img
                src={hospital_logo}
                alt={"ProviderLogo.svg"}
                style={{
                    // width: sm ? '40%' : lg ? '20%' : '23%'
                    width: '130px',
                    maxHeight: '130px',
                    objectFit: 'contain'
                }} /> : null} */}
            {hospital_logo ? <Grid sx={{ textAlign: 'center', width: '30%', height: '100px' }}>
                <img
                    src={hospital_logo}
                    alt={"ProviderLogo.svg"}
                    style={{
                        // width: sm ? '40%' : lg ? '20%' : '23%'
                        width: '100%',
                        height: '100%',
                        objectFit: 'contain'
                    }} />
            </Grid> : null}
            {/* <Typography
                    variant='subTitle2'
                    component={"div"}
                    sx={{
                        fontSize: '16px',
                        width: "100%",
                        textAlign: "center"
                    }}>A platform for digital recovery of small bills</Typography> */}

            <PaymentForm
                form={`Payment_Form`}
                onSubmit={handleSubmit}
                btnload={loadBtn}
                setBtnLoad={setLoadBtn}
            />

            {errorRes || (typeof errors == 'string' && errors) ? <CustomisedSnackBar
                message={errorRes || errors}
                open={errorRes || errors}
                onClose={() => errorRes ? setError(false) : dispatch(clearErrors())}
                // autoCloseDelay={2000}
                alertStyle={{
                    backgroundColor: '#F03249',
                    color: '#fff',
                    fontSize: '14px'
                }}
                severity={'error'}
            /> : null}
        </Grid>
    </Grid >
}

export default PayFormPage;