
import React from "react";
import { Grid, Typography, useMediaQuery } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import useStyles from "./styles";
import schema from "./schema";
import { useTheme } from "@mui/system";
import { useNavigate, useParams } from 'react-router-dom';
import routes from "../../utils/routes";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { clearSessionErrors } from "../../redux/session/actions";
import CustomisedSnackBar from "../../components/CustomisedSnackBar";
import { verifyOtp, createCustomErrors, clearSession, linkOpened } from "../../redux/session/actions";
import OtpForm from "../../components/OtpForm";
import Otp from '../../images/OtpViolet.svg';
import Spinner from "../../components/Spinner";
import store2 from "store2";

const otpForm = schema().otpForm().columns;

const OtpPage = () => {
    const [loadBtn, setLoadBtn] = React.useState(false);
    const { classes } = useStyles();
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const mdDown = useMediaQuery(theme.breakpoints.down('md'));
    const sm = useMediaQuery(theme.breakpoints.down('sm'));
    const lg = useMediaQuery(theme.breakpoints.up('lg'));
    const navigate = useNavigate();
    const [loading, setLoading] = React.useState(false);
    const [apiResponseReceived, setApiResponseReceived] = React.useState(false);
    const errors = useAppSelector(state => state?.session?.error);
    const hospital = useAppSelector(state => state?.session?.hospital) || {};
    const dispatch = useAppDispatch();
    const { id } = useParams();

    React.useEffect(() => {
        dispatch(clearSession());
        if (id) {
            setLoading(true);
            const payload = Object.assign({}, { patient_id: id });
            dispatch(linkOpened({
                payload: payload, loader: () => {
                    setLoading(false);
                    setApiResponseReceived(true);
                }
            }));
            store2.set('patient_id', id);
        }
    }, [id]);

    const nextStep = () => {

        navigate(routes.PAYMENT);
    }

    const handleSubmit = (data) => {
        setLoadBtn(true);
        const dateFormatted = data.otp_secret && data.otp_secret.includes(' / ') && data.otp_secret.split(' / ') || [];
        const outputDateString = dateFormatted.join('/');
        const payload = Object.assign({}, { otp_secret: outputDateString, patient_id: id || '' })
        if (id) {
            dispatch(verifyOtp({ payload: payload, nextStep: nextStep, loader: () => setLoadBtn(false) }));
        } else {
            dispatch(createCustomErrors({ error: 'Invalid patient id' }));
            setLoadBtn(false);
        }

    }

    return <Grid container sx={{
        flexDirection: 'column',
        height: '100%',
        justifyContent: 'center',
        backgroundColor: "rgb(248, 248, 251)",
        alignItems: 'center',
    }}>

        <Grid container
            sx={{
                gap: '12px',
                justifyContent: 'center',
                alignItems: 'center',
                padding: sm ? '0 10%' : md ? '0 25%' : '0 25%',
                flexDirection: 'column'
            }}
            className={classes.formContainer}
        >
            <Grid item
                sx={{
                    backgroundColor: '#F8F8FB',
                    borderRadius: '30px',
                    textAlign: 'center',
                }}>
                {apiResponseReceived && (hospital?.hospital_logo ? <img
                    src={hospital?.hospital_logo}
                    alt='Otp.svg'
                    style={{
                        width: '65%',
                        maxHeight: '150px',
                        objectFit: 'contain'
                    }} />
                    : <img src={Otp}
                        alt='Otp.svg'
                        style={{
                            width: '65%'
                        }} />)}
            </Grid>
            <Grid
                sx={{
                    display: 'flex',
                    flexFlow: 'column',
                    textAlign: 'center',
                    gap: '12px'
                }}>
                <Typography
                    component={"div"}
                    sx={{
                        color: '#344055',
                        fontSize: md ? '27px' : '20px',
                        width: "100%",
                        textAlign: "center",
                        fontFamily: 'Poppins-Medium'
                    }}>Two Step Verification</Typography>
                <Typography
                    component={"div"}
                    sx={{
                        // color: '#344055',
                        color: '#7F8CA5',
                        fontSize: md ? '16px' : '14px',
                        width: "100%",
                        textAlign: "center",
                    }}>In order to keep your data secure, we need to verify your identity.<br /><br /> Thank you for understanding.</Typography>
                {/* <Typography
                    component={"div"}
                    sx={{
                        fontSize: md ? '16px' : '14px',
                        width: "100%",
                        textAlign: "center",
                        color: '#7F8CA5'
                    }}>A text message with security code has been sent to your mobile phone</Typography> */}
            </Grid>
            <Grid
                sx={{
                    width: sm ? '100%' : lg ? '40%' : mdDown ? '80%' : '60%',
                    marginTop: '15px'
                }}
            >
                <OtpForm
                    form="Otp_Form"
                    fields={otpForm.filter(_ => _.editRecord)}
                    onSubmit={handleSubmit}
                    noNeedCancel={true}
                    createCustomErrors={(err) => dispatch(createCustomErrors({ error: err }))}
                    submitBtn={(props) => {
                        return <LoadingButton
                            loading={loadBtn}
                            type="submit"
                            variant="contained"
                            className={classes.loadingBtn}
                            fullWidth
                        >
                            Verify
                        </LoadingButton>
                    }
                    }
                />
            </Grid>
            {typeof errors == 'string' && errors ? <CustomisedSnackBar
                message={errors}
                open={errors}
                onClose={() => dispatch(clearSessionErrors())}
                autoCloseDelay={2000}
                alertStyle={{
                    backgroundColor: '#F03249',
                    color: '#fff',
                    fontSize: '14px'
                }}
                severity={'error'}
            /> : null}
            {loading ? <Spinner style={{
                position: "absolute",
                height: "100%",
                display: "flex",
                alignItems: "center",
            }} /> : null}

        </Grid>
    </Grid>
}

export default OtpPage;