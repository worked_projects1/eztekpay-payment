
export default function schema() {
    function otpForm() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'otp_secret',
                    label: 'Date of Birth',
                    placeholder: 'MM/DD/YYYY',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
            ]
        }
    }

    return {
        otpForm
    }
}

