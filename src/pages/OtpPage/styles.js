import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    // formContainer: {

    // },
    loadingBtn: {
        padding: theme.spacing(1, 2.5),
        fontSize: '14px',
        lineHeight: '1.5',
        fontFamily: 'Poppins-SemiBold',
        height: '50px',
        borderRadius: "50px",
        // backgroundColor: '#FFAA01',
        backgroundColor: '#385C9F',
        boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
        textTransform: 'capitalize',
        "& .MuiLoadingButton-loadingIndicator": {
            color: "#FFF"
        },
        // "& .Mui-disabled": {
        //     // backgroundColor: '#FFAA01',
        //     backgroundColor: '#385C9F'
        // }
    }
}));

export default useStyles;