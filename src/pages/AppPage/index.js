import React from "react";
import { Outlet, useLocation, useNavigate, matchPath } from 'react-router-dom';
import { useAppSelector } from '../../redux/hooks';
import routes from "../../utils/routes";
import store2 from "store2";

const AppPage = () => {
    const userOtp = useAppSelector(state => state?.session?.userOtp);
    const navigate = useNavigate();
    const { pathname } = useLocation();
    // const { id } = useParams();
    const matched = matchPath(pathname, routes.PAYMENT);
    const patient_id = store2.get('patient_id');

    React.useEffect(() => {
        if ((!userOtp && (matched && Object.keys(matched).length)) || pathname === '/') {
            if (patient_id) {
                navigate(`/${patient_id}`);
            } else {
                navigate(routes.HOME);
            }
        }
    }, [userOtp, pathname])
    return <Outlet />
}

export default AppPage;