import { Button, Grid, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Spinner from "../../components/Spinner";
import { useNavigate, useLocation, matchPath } from "react-router-dom";
import routes from "../../utils/routes";

function NotFound() {
    const [showPage, setShowPage] = useState(false);
    const navigate = useNavigate();
    const { pathname } = useLocation();
    const oldMatch = matchPath(routes.OLD_OTP_FORM, pathname);
    // console.log("oldmatch = ", oldMatch);

    useEffect(() => {
        if (pathname.includes('payment_link')) {
            if (oldMatch?.params?.id) {
                navigate(`/${oldMatch?.params?.id}`);
            } else {
                navigate(routes.HOME);
            }
        }
        setTimeout(() => setShowPage(true), 3000);
    }, []);


    return showPage ? <Grid container sx={{
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }}>
        <Grid sx={{
            textAlign: 'center'
        }}>
            <Typography variant='h6'>Not found</Typography>
            <Grid>
                <Link to='/'>
                    <Button sx={{
                        textTransform: 'none',
                        color: '#fff'
                    }}><Typography variant="subtitle2">Go to Home page</Typography></Button>
                </Link>
            </Grid>
        </Grid>
    </Grid> : <Spinner showHeight />

}

export default NotFound;

