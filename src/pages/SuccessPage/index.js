import { Grid, Typography } from "@mui/material";
import React from "react";
import SuccessAnimation from "../../components/SuccessAnimation";

const SuccessPage = () => {

    return <Grid container sx={{
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }}>
        <Grid sx={{
            textAlign: 'center'
        }}>
            {/* <Grid>
                <img src={Stars} alt='stars svg' />
                <img src={Success} alt='Success svg' />
                <img src={Stars} alt='stars svg' />
            </Grid> */}
            {/* <img src={StarsSuccess} alt='StarsSuccess svg' style={{
                width: '30%'
            }} /> */}
            <SuccessAnimation />
            <Typography sx={{
                fontFamily: 'Poppins-SemiBold',
                // color: "#2DCFC1",
                color: 'Group',
                fontSize: '26px '
            }}>
                Your payment has been successfully processed.
            </Typography>
        </Grid>
    </Grid>
}

export default SuccessPage;