
import React from 'react';
import { ThemeProvider, createTheme, responsiveFontSizes, } from '@mui/material/styles';
// import GlobalStyle from './globalStyles';
import CssBaseline from '@mui/material/CssBaseline';

const lightTheme = createTheme({
    palette: {
        mode: 'light',
    },
    button: {
        textTransform: 'none',
    },
    typography: {
        "fontFamily": "Poppins-Regular",
        "fontSize": 14,
        "fontWeightLight": 300,
        "fontWeightRegular": 400,
        "fontWeightMedium": 500,
        color: '#344055',
        h5: {
            "fontFamily": "Poppins-SemiBold",
            "textTransform": "Capitalize"
        }
    },
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    minWidth: 'max-content',
                    // backgroundColor: '#FFAA01',
                    backgroundColor: '#385C9F',
                    borderRadius: "50px",
                    // background: '#385C9F',
                    // background: "#FFAA01",
                    boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
                    '&.Mui-disabled': {
                        backgroundColor: "rgb(0 0 0 / 12%)",
                    },
                    '&:hover': {
                        // backgroundColor: '#FFAA01',
                        backgroundColor: '#385C9F'
                    },
                },
            },
        },
        // MuiOutlinedInput: {
        //     styleOverrides: {
        //         root: {
        //             "& .MuiOutlinedInput-notchedOutline": {
        //                 borderColor: "#E7E7E7",
        //             },
        //             "&:hover .MuiOutlinedInput-notchedOutline": {
        //                 borderRadius: "50px",
        //                 borderColor: "#E7E7E7",
        //                 borderWidth: 1
        //             },
        //             "&:focus .MuiOutlinedInput-notchedOutline": {
        //                 borderColor: "#E7E7E7",
        //             },
        //             "&:active .MuiOutlinedInput-notchedOutline": {
        //                 borderColor: "#E7E7E7",
        //             },
        //             "& .MuiOutlinedInput-colorSecondary": {
        //                 borderColor: "#E7E7E7",
        //             }

        //         }
        //     }
        // },
        MuiAlert: {
            styleOverrides: {
                standardSuccess: {
                    backgroundColor: 'green',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardError: {
                    backgroundColor: 'red',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardWarning: {
                    backgroundColor: 'orange',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardInfo: {
                    backgroundColor: 'grey',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                filledInfo: {
                    backgroundColor: 'grey', // Customize the background color for the info variant
                },

            },
        },
    }
});


const MUITheme = ({ children }) => {

    return (
        <ThemeProvider theme={responsiveFontSizes(lightTheme)}>
            {/* <GlobalStyle /> */}
            <CssBaseline />
            {children}
        </ThemeProvider>
    );
};

export default MUITheme;