/**
 * OtpForm
 */

import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ImplementationFor } from '../EditRecordForm/utils';
import { Grid, Button, Typography } from '@mui/material';
import Styles from './styles';
import LoadingButton from '@mui/lab/LoadingButton';
import { Field, reduxForm } from 'redux-form';
import { normalize } from '../../utils/tools';
import moment from 'moment';

const dateValidation1 = (val) => {
    var date = val;
    var result = '';
    var splittedDate = date && date.includes(' / ') && date.split(' / ');
    var dateFormat = 'MM/DD/YYYY';

    if (splittedDate && splittedDate.length === 3) {
        var formattedDate = `${splittedDate[0]}/${splittedDate[1]}/${splittedDate[2]}`;

        if (!moment(formattedDate, dateFormat, true).isValid()) {
            result = 'Invalid date';
        }
    } else {
        result = 'Invalid date';
    }
    return result
}

const validate = (values, props) => {
    const errors = {};
    const { fields } = props;
    const requiredFields = fields.filter(s => s.mandatory).map(e => e.value);

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    if (values.otp_secret && dateValidation1(values.otp_secret)) {
        errors.otp_secret = dateValidation1(values.otp_secret);
    }

    return errors;
};

/**
 * 
 * @param {object} props 
 * @returns 
 */

const OtpForm = (props) => {
    const { classes } = Styles();
    const { handleSubmit, pristine, submitting, fields, parentPath, locationState, btnLabel, invalid, destroy, spinner, cancelBtn, updateBtn, submitBtn, enableSubmitBtn, noNeedCancel, createCustomErrors } = props;

    const [count, setCount] = React.useState(15);
    let timerInterval = React.useRef();

    useEffect(() => {
        if (count === 0) {
            clearInterval(timerInterval.current);
            setCount(15);
        }
    }, [count]);

    const activateTimer = () => {
        // timerInterval.current = setInterval(() => {
        //     setCount((prevSeconds) => prevSeconds - 1);
        // }, 1000);
        createCustomErrors("This feature is currently not functional");

    };

    const dispSecondsAsMins = (seconds) => {
        const mins = Math.floor(seconds / 60);
        const seconds_ = seconds % 60;
        return `${mins.toString().padStart(2, '0')}:${seconds_.toString().padStart(2, '0')}`;
    };

    useEffect(() => {
        return () => destroy();
    }, []);

    return (<Grid>
        <form onSubmit={handleSubmit}>
            <Grid container
                sx={{
                    gap: '5px',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}
            >
                {(fields || []).map((field, index) => {
                    const InputComponent = ImplementationFor[field.type];
                    const additionClass = {
                        ...(field.value == "otp_secret" && {
                            customLabel: classes.customLabel
                        } || {})
                    }
                    return <Grid key={index} item xs={12} sx={{
                        width: '100%'
                    }}><Field
                            name={field.value}
                            label={field.label}
                            component={InputComponent}
                            // type={field.type}
                            // {...formik.getFieldProps(field.value)}
                            {...field}
                            {...additionClass}
                            placeholder={field.placeholder}
                            options={field.options}
                            normalize={normalize(field)}
                        />
                    </Grid>
                })}

                <Grid className={classes.footer}>
                    {submitBtn && typeof submitBtn === 'function' ? React.createElement(submitBtn, Object.assign({}, { ...props, classes })) : <Button
                        type="submit"
                        disabled={!enableSubmitBtn && (pristine || submitting) || !enableSubmitBtn && (!pristine && invalid)}
                        variant="contained"
                        color="primary"
                        className={updateBtn ? updateBtn : classes.submitBtn}>
                        {((submitting || spinner) && <LoadingButton loading variant="outlined" />) || btnLabel || 'Update'}
                    </Button>}
                    {!noNeedCancel ? (cancelBtn && React.createElement(cancelBtn)) ||
                        <Link
                            to={`/${(parentPath && locationState && parentPath) || ''}`}
                            state={locationState}
                        >
                            <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                className={classes.cancelBtn}>
                                Cancel
                            </Button>
                        </Link> : null}
                </Grid>
                {/* <Grid sx={{
                    paddingTop: '12px'
                }}>
                    {count === 15 ? <Button
                        onClick={() => activateTimer()}
                        className={classes.linkColorBtn}
                    >
                        Resend OTP
                    </Button> :
                        <Typography>
                            {dispSecondsAsMins(count)}
                        </Typography>}
                </Grid> */}
            </Grid>
        </form>
    </Grid>)
}

export default reduxForm({
    form: 'otp_form',
    enableReinitialize: true,
    validate,
    touchOnChange: true,
    destroyOnUnmount: true,
    // forceUnregisterOnUnmount: true
})(OtpForm);