

import { makeStyles } from 'tss-react/mui';
import { createTheme } from "@mui/material";
const theme = createTheme();

const useStyles = makeStyles()(() => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(4)
    },
    forms: {
        width: '100%',
        marginTop: '5px'
    },
    submitBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        // marginRight: '15px',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        textTransform: 'capitalize',
        backgroundColor: 'gray !important'
    },
    footer: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
        marginTop: '10px'
    }
}));


// TODO jss-to-tss-react codemod: usages of this hook outside of this file will not be converted.
export default useStyles;