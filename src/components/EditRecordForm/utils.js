
import CustomInput from '../CustomInput';
import StripeField from '../StripeField';
// import PhoneNumberField from '../PhoneNumberField';
// import PinInput from '../PinInput';

export const ImplementationFor = {
    input: CustomInput,
    stripe: StripeField,
    // phone: PhoneNumberField,
    pin: CustomInput
};
