/**
 * 
 * Edit Record Form
 * 
 */

import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ImplementationFor } from './utils';
import { Grid, Button, useMediaQuery } from '@mui/material';
import Styles from './styles';
import LoadingButton from '@mui/lab/LoadingButton';
import { Field, reduxForm } from 'redux-form';
import { isPossiblePhoneNumber } from 'react-phone-number-input/input';
import { useTheme } from "@mui/system";

const validate = (values, props) => {
    const errors = {};
    const { fields } = props;
    const requiredFields = fields.filter(s => s.mandatory).map(e => e.value);
    const phoneFields = fields.filter(s => s.type == "phone").map(e => Object.assign({}, { value: e.value, max: e.max }));


    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    phoneFields.forEach(field => {
        if (values[field.value]) {
            if (!isPossiblePhoneNumber(values[field.value])) {
                errors[field.value] = 'Invalid Phone Number'
            }
        }
    })
    // }
    return errors;
};

/**
 * 
 * @param {object} props 
 * @returns 
 */


function EditRecordForm(props) {

    const { classes } = Styles();
    const { handleSubmit, pristine, submitting, fields, parentPath, error, locationState, btnLabel, invalid, destroy, spinner, cancelBtn, updateBtn, submitBtn, enableSubmitBtn, noNeedCancel, onFormValuesChange, initialFormValues } = props;

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.down('sm'));

    useEffect(() => {
        return () => destroy();
    }, []);

    return (<div style={{
        padding: sm ? "0px" : '0px 15%'
    }}>
        <form onSubmit={handleSubmit}>
            <Grid container
                sx={{
                    gap: '5px',
                    justifyContent: 'center'

                }}
            >
                {(fields || []).map((field, index) => {
                    const InputComponent = ImplementationFor[field.type];
                    return <Grid key={index}
                        item
                        xs={12}
                        sx={{
                            width: '100%'
                        }}
                    >
                        <Field
                            name={field.value}
                            label={field.label}
                            component={InputComponent}
                            // type={field.type}
                            {...field}
                            placeholder={field.placeholder}
                            options={field.options}
                        />
                    </Grid>
                })}
            </Grid>
            <Grid className={classes.footer}>
                {submitBtn && typeof submitBtn === 'function' ? React.createElement(submitBtn, Object.assign({}, { ...props, classes })) : <Button
                    type="submit"
                    disabled={!enableSubmitBtn && (pristine || submitting) || !enableSubmitBtn && (!pristine && invalid)}
                    variant="contained"
                    color="primary"
                    className={updateBtn ? updateBtn : classes.submitBtn}>
                    {(submitting || spinner) && <LoadingButton loading variant="outlined" /> || btnLabel || 'Update'}
                </Button>}
                {!noNeedCancel ? cancelBtn && React.createElement(cancelBtn) ||
                    <Link
                        to={`/${parentPath && locationState && parentPath || ''}`}
                        state={locationState}
                    >
                        <Button
                            type="button"
                            variant="contained"
                            color="primary"
                            className={classes.cancelBtn}>
                            Cancel
                        </Button>
                    </Link> : null}
            </Grid>
        </form>
    </div>)

}

export default reduxForm({
    form: 'edit_Record',
    enableReinitialize: true,
    validate,
    touchOnChange: true,
    destroyOnUnmount: true,
})(EditRecordForm);