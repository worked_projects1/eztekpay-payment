import React from 'react';

import DashboardIcon from '@mui/icons-material/Dashboard';
import Visa from '../../images/stripe/PaymentMethod/Visa.svg';
import Master from '../../images/stripe/PaymentMethod/Mastercard.svg';
import Amex from '../../images/stripe/PaymentMethod/Amex.svg';
import Discover from '../../images/stripe/PaymentMethod/Discover.svg';
import Card1 from '../../images/stripe/PaymentMethod/DinersClub.svg';
import UnionPay from '../../images/stripe/PaymentMethod/UnionPay.svg';
import Stripe from '../../images/stripe/PaymentMethod/Stripe.svg';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function Icons(props) {

    // const classes = Styles();

    switch (props.type) {

        case 'visa':
            return <img src={Visa} alt='visa png' {...props}
                loading="lazy"
            />
        case 'masterCard':
            return <img src={Master} alt='masterCard png' {...props}
                loading="lazy"
            />
        case 'amex':
            return <img src={Amex} alt='amex png' {...props}
                loading="lazy"
            />
        case 'discover':
            return <img src={Discover} alt='discover png' {...props}
                loading="lazy"
            />
        case 'card1':
            return <img src={Card1} alt='card1 png' {...props}
                loading="lazy"
            />
        case 'union_pay':
            return <img src={UnionPay} alt='union_pay png' {...props}
                loading="lazy"
            />
        case 'stripe':
            return <img src={Stripe} alt='stripe png' {...props}
                loading="lazy"
            />
        default:
            return <DashboardIcon {...props} sx={{
                color: props.color
            }} />
    }
}