/**
 * 
 * Radio Box
 * 
 */

import React from 'react';
import useStyles from './styles';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import { FormControlLabel, FormLabel } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import { Grid } from '@mui/material';
import UncheckRadio from '../../images/UncheckRadio.svg';
import CheckedRadio from '../../images/CheckedRadio.svg';

export default function ({ input, label, options, disabled, row, meta: { touched, error, warning }, defaultValue, submitButtonName, customStyle }) {// eslint-disable-line
    const { classes } = useStyles();

    const { name, value, } = input;// eslint-disable-line
    const isPreDefinedSet = Array.isArray(options);


    return (
        <Grid className={classes.checkboxField} >
            <FormControl component="fieldset">
                <FormLabel key="demo-customized-radios" name="sample"
                    sx={Object.assign({}, {
                        fontFamily: 'Poppins-SemiBold'
                    }, customStyle || {})}
                >
                    {label}
                </FormLabel>
                <RadioGroup
                    row={row}
                    name={name}
                    value={value || ""}
                    className={classes.radioGroup}
                    onChange={(e) => {
                        input.onChange(e.target.value)
                    }}>
                    {isPreDefinedSet ? (options || []).map((opt, index) =>
                        <FormControlLabel
                            key={index}
                            value={opt.value}
                            name={opt.value}
                            label={opt.label}
                            control={
                                <Radio
                                    icon={<img src={UncheckRadio} alt='UncheckRadio svg' style={{ width: '25px' }} />}
                                    checkedIcon={<img src={CheckedRadio} alt='CheckedRadio svg' style={{ width: '25px' }} />}
                                    checked={
                                        opt.value == value}
                                    classes={{ root: classes.radio, checked: classes.checked }}
                                />}
                            disabled={disabled ? true : false}
                            labelPlacement="end"
                            className={classes.radioLabel}
                        />
                    ) : null}
                </RadioGroup>
            </FormControl>
            <div className={classes.error}>
                {touched && ((error && <span style={{ color: "red", fontSize: '14px' }}>{error}</span>) || (warning && <span style={{ color: "red", fontSize: '14px' }}>{warning}</span>))}
            </div>
        </Grid>
    );
}
