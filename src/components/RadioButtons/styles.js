import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    checkboxField: {
        width: '100%',
        '& .MuiFormControl-root': {
            '& .MuiFormLabel-root.Mui-focused': { color: '#344055', },
            '& .MuiFormLabel-root': {
                color: '#344055',
                fontSize: '15px'
            },
        },
    },
    radioLabel: {
        width: '100%',
        fontSize: '18px',
        margin: '0px',
        '&.MuiButtonBase-root-MuiRadio-root': {
            padding: '5px'
        },
        '&.MuiFormControlLabel-label': {
            width: '100%',
        },
        '& .MuiTypography-root': {
            width: '100%',
        }
    },
    radioGroup: {
        marginTop: '1em'
    }
}));

export default useStyles;