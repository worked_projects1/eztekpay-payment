import * as React from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import { Grid, FormControlLabel } from '@mui/material';
import RectangleChecked from '../../images/Checked.svg';
import CheckedViolet from '../../images/CheckedViolet.svg';
import Rectangle from '../../images/Rectangle.svg';
import useStyles from './styles';

function EnhancedTableHead(props) {
    const { onSelectAllClick, numSelected, rowCount, columns } =
        props;
    const { classes } = useStyles();

    return (
        <TableHead>
            <TableRow
                className={classes.tableHeaderRow}
            >
                <TableCell
                    // padding="checkbox"
                    className={classes.tableHeaderData}
                    sx={{
                        // width: '8px',
                        textAlign: 'center',
                        minWidth: '16%',
                    }}>
                    <FormControlLabel
                        className={classes.checkboxLabel}
                        control={<Checkbox
                            color="primary"
                            // indeterminate={numSelected > 0 && numSelected < rowCount}
                            checked={rowCount > 0 && numSelected === rowCount}
                            onChange={onSelectAllClick}
                            inputProps={{
                                'aria-label': 'select all desserts',
                            }}
                            checkedIcon={<img src={CheckedViolet} alt="checkbox" style={{ width: '20px' }} />}
                            icon={<img src={Rectangle} alt="uncheckbox" style={{ width: '20px' }} />}
                        />}
                        label="Select All"
                        labelPlacement="top"
                    />
                </TableCell>
                {columns.map((headCell) => (
                    <TableCell
                        key={headCell.value}
                        sx={{
                            minWidth: headCell?.width ? headCell?.width : 'min-content',
                        }}
                        className={classes.tableHeaderData}
                    // align={headCell.numeric ? 'right' : 'left'}
                    // padding={headCell.disablePadding ? 'none' : 'normal'}
                    // sortDirection={orderBy === headCell.id ? order : false}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

export default function TableCheckboxField(props) {
    const { records, columns, input, label, disabled, meta: { touched, error, warning }, defaultValue, customStyle } = props;
    const { name, value, onChange } = input;

    const [selected, setSelected] = React.useState((!value || value == "") ? [] : JSON.parse(value));
    const { classes } = useStyles();

    const tableColumns = columns;
    let rows = records.map((record) => Object.assign(
        {},
        record
    ));

    tableColumns.forEach((column) => {
        rows = rows.map((row) => Object.assign(
            row,
            {
                [column.value]:
                    column.html ? column.html(row, row[column.value]) :
                        row[column.value] || "",
            },
        ));
    });

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelected = rows.map((n) => n.id);
            setSelected(newSelected);

            input.onChange(JSON.stringify(newSelected));
            return;
        }
        input.onChange(JSON.stringify([]));
        setSelected([]);
    };

    const handleClick = (event, id) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
        // const selectedRecords = (records || []).filter(item => newSelected.includes(item?.invoice_number))
        input.onChange(JSON.stringify(newSelected));
    };

    const isSelected = (id) => selected.indexOf(id) !== -1;

    const renderCell = (row, i) => columns.map((col, ind) => {
        return <TableCell
            key={`${col.value}-${i}-${ind}`}
            sx={{
                minWidth: col?.width ? col?.width : 'auto',
                maxWidth: col?.width ? col?.width : 'auto',
            }}
            className={col?.value == "invoice_amount" ? classes.tableAmountData : classes.tableBodyData}
        >
            {row[col.value]}
        </TableCell>
    })

    return (
        <Box
        >
            <Paper
                // sx={{ width: '100%', mb: 2 }}
                className={classes.paper}
            >
                <TableContainer className={classes.tableContainer}>
                    <Table
                        // sx={{ minWidth: 750 }}
                        aria-labelledby="tableTitle"
                        // size={dense ? 'small' : 'medium'}
                        size='small'
                        name={name}
                        stickyHeader
                        className={classes.table}
                    >
                        <EnhancedTableHead
                            numSelected={selected.length}
                            onSelectAllClick={handleSelectAllClick}
                            rowCount={rows.length}
                            columns={columns}
                        />
                        <TableBody>
                            {(rows || []).map((row, index) => {
                                const isItemSelected = isSelected(row.id);
                                const labelId = `enhanced-table-checkbox-${index}`;

                                return (
                                    <TableRow
                                        hover
                                        // onClick={(event) => handleClick(event, row.id)}
                                        // role="checkbox"
                                        // aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={row.id}
                                        // selected={isItemSelected}
                                        sx={{ cursor: 'pointer' }}
                                        // className={classes.tableBodyRow}
                                        classes={{
                                            // root: classes.tableRowRoot,
                                            selected: classes.tableRowSelected,
                                        }}
                                    >
                                        <TableCell
                                            id={labelId}
                                            style={{
                                                // minWidth: '50px'
                                                minWidth: '16%',
                                                textAlign: 'center'
                                            }}
                                            className={classes.tableBodyData}
                                        >
                                            <Checkbox
                                                color="primary"
                                                checked={isItemSelected}
                                                inputProps={{
                                                    'aria-labelledby': labelId,
                                                }}
                                                checkedIcon={<img src={CheckedViolet} alt="checkbox" style={{ width: '20px' }} />}
                                                icon={<img src={Rectangle} alt="uncheckbox" style={{ width: '20px' }} />}
                                                onChange={(e) => handleClick(e, row.id)}
                                            />
                                        </TableCell>
                                        {renderCell(row, index)}
                                    </TableRow>
                                );
                            })}

                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <Grid className={classes.errorContainer}>
                {touched && ((error && <span className={classes.error}>{error}</span>) || (warning && <span>{warning}</span>))}
            </Grid>
        </Box>
    );
}