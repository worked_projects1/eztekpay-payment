import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    tableBodyData: {
        backgroundColor: 'transparent',
        color: '#fff',
        padding: '5px',
        fontFamily: 'Poppins-Regular',
        fontSize: '12px',
        wordBreak: 'break-word',
        border: 'none'
    },
    tableHeaderData: {
        // backgroundColor: '#005BAE',
        backgroundColor: '#385C9F',
        color: '#fff',
        padding: '5px',
        fontFamily: 'Poppins-SemiBold',
        fontSize: '12px',
        border: 'none',
        borderStyle: "dashed",
        borderWidth: "0px 0px 2px 0px",
        borderColor: '#FFFFFF80',
        // borderColor: "#1B92AC",
    },
    tableAmountData: {
        backgroundColor: 'transparent',
        color: '#fff',
        padding: '5px',
        fontFamily: 'Poppins-SemiBold',
        fontSize: '14px',
        border: 'none'
    },
    tableHeaderRow: {
        // maxHeight: '10px'
    },
    tableRowSelected: {
        // backgroundColor: 'transparent !important', //.MuiTableRow-root-tableBodyRow
        // "& .Mui-selected": {
        backgroundColor: 'transparent !important',
        // }
    },
    paper: {
        backgroundColor: 'transparent',
        width: '100%',
        boxShadow: 'none'
    },
    table: {
        border: 'none',
        // borderStyle: "dashed",
        // borderWidth: "2px 0px",
        // borderColor: "#1B92AC",
    },
    tableContainer: {
        // maxHeight: '250px',
        maxHeight: '325px',
        overflow: 'auto',
        maxWidth: '100%',
        // overflowX: 'hidden',
        "&::-webkit-scrollbar": {
            width: 6,
            height: 6,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '30px'
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#3c89c9",
            borderRadius: '30px'
        }
        // "&::-webkit-scrollbar": {
        //     width: "5px",
        //     height: '5px',
        //     borderRadius: '50px'
        // },

        // /* Track */
        // // "&::-webkit-scrollbar-track": {
        // //     background: "#f1f1f1",
        // // },

        // /* Handle */
        // "&::-webkit-scrollbar-thumb": {
        //     background: "#888",
        //     width: '200px'
        // },

        // /* Handle on hover */
        // "&::-webkit-scrollbar-thumb:hover": {
        //     background: "#555"
        // },
    },
    box: {
        /* width */
        // "&::-webkit-scrollbar": {
        //     width: "10px"
        // },

        // /* Track */
        // "&::-webkit-scrollbar-track": {
        //     background: "#f1f1f1",
        // },

        // /* Handle */
        // "&::-webkit-scrollbar-thumb": {
        //     background: "#888",
        // },

        // /* Handle on hover */
        // "&::-webkit-scrollbar-thumb:hover": {
        //     background: "#555"
        // },
        // overflow: 'auto'
    },
    error: {
        fontSize: '12px',
        color: 'red',
        marginLeft: '18px',
        lineHeight: '15px',
        display: 'block'
    },
    errorContainer: {
        height: '1em',
        // marginBottom: '0.5em',
    },
    checkboxLabel: {
        // display: 'flex',
        // flexDirection: 'column',
        margin: 0,
        '& .MuiFormControlLabel-label': {
            fontFamily: 'Poppins-SemiBold',
            fontSize: '12px',
            color: '#fff',
        }
    }
}));

export default useStyles;