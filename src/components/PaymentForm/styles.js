

import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    formControlLabel: {
        margin: 0,
    },
    textSize: {
        fontSize: '14px',
    },
    error: {
        fontSize: '14px',
        color: 'red'
    },
    linkColorBtn: {
        // color: '#2DCFC1',
        color: '#385C9F',
        // backgroundColor: 'transparent !important',
        backgroundColor: '#fff !important',
        textTransform: 'capitalize',
        border: 'none',
        boxShadow: 'none',
        fontFamily: 'Poppins-SemiBold',
        padding: '0px',
        fontSize: '15px'
    },
    loadingButton: {
        // width: '80%',
        [`${theme.breakpoints.down('sm')}`]: {
            width: '100%',
        },
        padding: theme.spacing(1.5, 2),
        fontSize: '14px',
        lineHeight: '1.5',
        fontFamily: 'Poppins-SemiBold',
        height: '50px',
        borderRadius: "50px",
        // backgroundColor: '#FFAA01 !important',
        backgroundColor: '#FFF !important',
        boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
        textTransform: 'capitalize',
        color: "#292929",
        // "& .MuiLoadingButton-loadingIndicator": {
        //     color: "#292929"
        // },
        // margin: '10% 0 0 0'
        // marginBottom: '-1em',
        "& .Mui-disabled": {
            // backgroundColor: '#FFAA01',
            backgroundColor: "#fff !important",
        },
        // '& .MuiLoadingButton-root-loadingButton.Mui-disabled': {

        // }
    },
    loadingButtoninForm: {
        width: '80%',
        [`${theme.breakpoints.down('sm')}`]: {
            width: '100%',
        },
        padding: theme.spacing(1.5, 2),
        fontSize: '14px',
        lineHeight: '1.5',
        fontFamily: 'Poppins-SemiBold',
        height: '50px',
        borderRadius: "50px",
        // backgroundColor: '#FFAA01 !important',
        backgroundColor: '#385C9F',
        boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
        textTransform: 'capitalize',
        color: "#fff",
        // "& .MuiLoadingButton-loadingIndicator": {
        //     color: "#fff"
        // },
        "& .Mui-disabled": {
            // backgroundColor: '#FFAA01',
            backgroundColor: `rgb(0 0 0 / 12%) !important`,
        },
    },
    linkColor: {
        // color: '#2DCFC1',
        color: '#385C9F',
        textDecoration: 'none',
        fontFamily: 'Poppins-SemiBold'
    },
    labelClass: {
        top: '17px',
        textWrap: 'wrap'
    },
    customLabel: {
        fontSize: '15px',
        color: '#344055',

        fontWeight: 'normal',
        '& span': {
            // color: '#FFAA01',
            color: '#385C9F',
            marginLeft: '10px',
        }
    },
    textContainer: {
        display: 'flex',
        alignItems: 'flex-end',
        flexBasis: '60%'
    }
}));


export default useStyles;