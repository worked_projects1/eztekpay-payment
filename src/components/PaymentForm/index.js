/**
 * 
 * Login Form
 * 
 */


import React, { useMemo } from 'react';
import { Grid } from '@mui/material';
import { paymentForm } from './schema'
import Form from './Form';
import { getFormValues } from 'redux-form';
import { useAppSelector } from '../../redux/hooks';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function PaymentForm(props) {
    const {

        locationState = {},
        errorMessage,
        clearCache,
        btnload,
        setBtnLoad,
        onSubmit,
        customMessage
    } = props;

    const fields = (paymentForm().columns).filter(_ => _.editRecord);
    const userData = useAppSelector(state => state?.session?.userData);
    const selector = useAppSelector(state => getFormValues('payment_Form')(state));

    const invoice_details = userData?.invoice_details ?? [];
    const intitalSelectedInvoice = (invoice_details || []).map(item => item?.invoice_number && item?.invoice_number || "");
    const decimalPlaces = 2;

    const handleSubmit = (e) => {
        setBtnLoad(true);
        let paid_amount = 0;
        let amount_details = {};
        if (selector?.amount_details && JSON.parse(selector?.amount_details).length) {
            const selected = JSON.parse(selector?.amount_details);
            const selectedInvoices = (invoice_details || []).filter(item => selected.includes(item?.invoice_number));
            const sumOfSelectedInvoice = selectedInvoices.reduce((sum, item) => {
                return sum + (item?.invoice_amount || 0);
            }, 0);
            paid_amount = Math.round(sumOfSelectedInvoice * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces)
            amount_details = selectedInvoices;
        }
        onSubmit(Object.assign({}, e, { paid_amount: paid_amount, amount_details: amount_details }));
    }

    return (
        <Grid sx={{
        }}>
            <Form
                initialValues={Object.assign({}, { amount_details: JSON.stringify(intitalSelectedInvoice) })}
                onSubmit={handleSubmit}
                form='payment_Form'
                fields={fields}
                btnload={btnload}
                setBtnLoad={setBtnLoad}
                locationState={locationState}
                errorMessage={errorMessage}
                clearCache={clearCache}
                customMessage={customMessage}
                userData={userData}
            />
        </Grid>
    )
}

export default PaymentForm;