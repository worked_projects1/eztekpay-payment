/**
 * 
 * Login Form
 * 
 */


import React, { useEffect, memo } from 'react';
import { Grid, Typography, useMediaQuery } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import useStyles from './styles';
import { ImplementationFor } from '../EditRecordForm/utils';
import { useTheme } from "@mui/system";
import { Field, reduxForm, formValueSelector, } from 'redux-form';
import { connect } from 'react-redux';
// import { getByZipCode } from 'zips';
import moment from 'moment';
import { invoiceDetailsColumns } from './schema';
import TableCheckboxField from '../TableCheckboxField';
// import { zipCodes } from '../../localData/zipcodes';
import { normalize } from '../../utils/tools';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { getZipCode } from '../../redux/zipCodes/actions';
import { AsYouType } from 'libphonenumber-js';
// import ProviderLogo from '../../images/provider.png';

// const zipMap = new Map(zipCodes);

const validate = (values, props) => {
    const errors = {};
    const { fields, userData } = props;
    const requiredFields = fields.filter(s => s.mandatory).map(e => e.value);
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
        if (!values["amount_details"] || (values["amount_details"] && !JSON.parse(values["amount_details"]).length)) {
            errors["amount_details"] = 'Required'
        }
    })
    return errors;

};

/**
 * 
 * @param {object} props 
 * @returns 
 */
let Form = memo(function Form(props) {
    const { handleSubmit, btnload, destroy, fields, hasValues, change, userData } = props;
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.down('sm'));
    const md = useMediaQuery(theme.breakpoints.down('md'));
    const lg = useMediaQuery(theme.breakpoints.down('lg'));
    const xl = useMediaQuery(theme.breakpoints.up('xl'));
    const zipcodes = useAppSelector(state => state?.zipCodes?.zipCodeData);
    const dispatch = useAppDispatch();

    const invoice_details = userData?.invoice_details ?? [];
    const commaSeparatedInvoiceNumber = (invoice_details || []).map(item => item?.invoice_number).join(', ');
    const commaSeperatedDates = (invoice_details || []).map(item => item?.invoice_date && moment(item?.invoice_date).format('MM/DD/YYYY')).join(', ');
    const sum_of_pending_amounts = (invoice_details || []).reduce((sum, item) => {
        return sum + (item?.invoice_amount || 0);
    }, 0);
    // const decimalPlaces = 2;
    // const showAmount = Math.round(sum_of_pending_amounts * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces);
    const showAmount = parseFloat(sum_of_pending_amounts || 0).toFixed(2);
    const [payableAmount, setPayableAmount] = React.useState(sum_of_pending_amounts);
    // const hospital_logo = userData?.hospital_details?.logo_file || ""
    // const [zipCodes, setZipCodes] = React.useState([]);
    const { classes } = useStyles();
    const hospital_phone_number = new AsYouType('US').input(userData?.hospital_details?.phone_number || "");

    useEffect(() => {
        const fetchCityState = () => {
            dispatch(getZipCode());
        };
        fetchCityState();
    }, []);

    useEffect(() => {
        return () => destroy();
    }, []);

    useEffect(() => {
        if (hasValues?.amount_details && JSON.parse(hasValues?.amount_details).length) {
            const selected = JSON.parse(hasValues?.amount_details);
            const selectedInvoices = (invoice_details || []).filter(item => selected.includes(item?.invoice_number));
            const sumOfSelectedInvoice = selectedInvoices.reduce((sum, item) => {
                return sum + (item?.invoice_amount || 0);
            }, 0);
            // let showAmount = Math.round(sumOfSelectedInvoice * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces);
            let showAmount = parseFloat(sumOfSelectedInvoice || 0).toFixed(2);
            setPayableAmount(showAmount);
        } else {
            setPayableAmount(0);
        }
    }, [hasValues?.amount_details])

    const postalChange = async (newZipCode) => {
        // const npmlocation = getByZipCode(newZipCode);
        // console.log("newZipCode = ", newZipCode, "zipcodes = ", zipcodes);
        let location = "";
        if (newZipCode && newZipCode.length == 5) {
            location = zipcodes.get(Number(newZipCode));
        }
        if (location) {
            change('city', location?.city);
            change('state', location?.state);
        } else {
            change('city', "");
            change('state', "");
        }
    }

    const fieldsContainer = <Grid item
        sx={{
            width: sm || md ? '100%' : '65%',
            display: 'flex',
            flexDirection: 'column',
            borderRadius: "12px",
            background: "#FFF",
            boxShadow: "0px 0px 20px 0px rgba(0, 0, 0, 0.10)",
            margin: sm || md ? '7% 0 0 0' : "2% -33px 2% 0",
            justifyContent: 'center'
        }}
    >
        <Grid container sx={{
            flexFlow: sm || md ? 'column' : 'row'
        }}>
            <Grid item sx={{
                flex: 1,
                padding: sm ? '7%' : md ? '7%  10%' : lg ? '7% 10%' : xl ? '7% 10%' : '7% 13%',

            }}>
                {(fields || []).map((field, index) => {
                    const InputComponent = ImplementationFor[field.type];
                    const disableFunc = {
                        ...(field.type === 'pin' && {
                            onChange: postalChange
                        })
                    }
                    return <Grid key={index} sx={{
                    }}>
                        <Field
                            key={index}
                            name={field.value}
                            label={field.label}
                            component={InputComponent}
                            placeholder={field.placeholder}
                            options={field.options}
                            normalize={normalize(field)}
                            {...disableFunc}
                        />
                    </Grid>
                })}
            </Grid>
            {!sm || !md ? <Grid item sx={{
                width: '33px'
            }}>

            </Grid> : null}
            {sm || md ? <Grid item sx={{
                padding: sm ? '0 7% 7% 7%' : md ? '0 10% 7%  10%' : lg ? '7% 10%' : xl ? '7% 10%' : '7% 13%',
                flexDirection: 'column',
                maxwidth: '300px'
            }}>
                <LoadingButton
                    className={classes.loadingButtoninForm}
                    loading={btnload}
                    type='submit'
                    fullWidth
                    variant='contained'
                    sx={{
                        '&.Mui-disabled': {
                            backgroundColor: "rgb(0 0 0 / 12%) !important",
                        },
                    }}
                    // color='primary'
                    disabled={sum_of_pending_amounts === 0 || btnload}
                >{`Pay $${payableAmount || 0}`}
                </LoadingButton>
                {/* <Typography variant="subTitle1" component={"div"} sx={{
                    lineHeight: 1.5,
                    fontSize: '14px',
                    fontFamily: 'Poppins-Regular',
                    textAlign: 'center',
                    color: '#344055',
                    marginTop: sm || md ? '7%' : '10%',
                }}>
                    {`In case you have any questions regarding your bill, please contact at ${hospital_phone_number}.`}
                </Typography> */}
            </Grid>

                : null}
        </Grid>

    </Grid>;

    return (
        <Grid sx={{
        }}>
            <form onSubmit={handleSubmit} className={classes.form} noValidate>
                <Grid container
                    sx={{
                        flexFlow: sm || md ? 'column' : 'row',
                        paddingTop: '3%',
                        // alignItems: 'center'
                    }}>

                    {!sm && !md ? fieldsContainer : null}


                    <Grid item
                        sx={{
                            width: sm || md ? '100%' : '47%',
                            padding: '3%',
                            display: 'flex',
                            flexDirection: 'column',
                            borderRadius: "12px",
                            // background: "#005BAE",
                            backgroundColor: '#385C9F',
                            boxShadow: "0px 0px 50px 0px rgba(0, 0, 0, 0.20)"
                        }}
                    >
                        <Grid sx={{
                            paddingBottom: sm || md ? '3%' : '10%',
                            display: 'flex',
                            alignItems: 'center',
                            flexDirection: 'column',
                            justifyContent: 'flex-end',
                            flexBasis: '60%',
                            // flexBasis: (invoice_details && invoice_details.length == 1) ? '60%' : 'auto'
                        }}
                        // className={(invoice_details && invoice_details.length == 1) ? classes.textContainer : classes.dummy}
                        // className={classes.textContainer}
                        >
                            {/* {hospital_logo ? */}
                            {/* <Grid
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    paddingBottom: '5%'
                                }}
                            > */}
                            {/* <Grid sx={{
                                width: '35%'
                            }}> */}
                            {/* {hospital_logo ? <img src={hospital_logo} alt='hospital logo' style={{
                                // width: '100%',
                                objectFit: 'contain',
                                width: '100px',
                                height: '100px',
                                // width: '35%',
                                marginBottom: '10%',
                                // height: '80px'
                                // display: 'none'
                            }} id='hospital_logo' /> : null} */}
                            {/* </Grid> */}
                            {/* </Grid> */}
                            {/* : null} */}
                            <Typography variant="subTitle1" component={"div"} sx={{
                                lineHeight: 1.5,
                                fontSize: '14px',
                                fontFamily: 'Poppins-Regular',
                                textAlign: 'center',
                                color: '#fff'
                            }}
                                dangerouslySetInnerHTML={{

                                    __html: `Hi <b>${userData?.patient_name}</b>, <br/><br />
                                    You currently owe $${(sum_of_pending_amounts && showAmount) || 0}. For your convenience, we are providing you with another option for payment. <br/><br />
                                    Please take a moment to pay today. <br/><br />
                                    All transactions are securely processed through our payment gateway.`

                                    // -----------------Second time change ------------
                                    // __html: `Hi <b>${userData?.patient_name}</b>, <br/><br />
                                    // You currently have $${(sum_of_pending_amounts && showAmount) || 0} owed to ${userData?.hospital_name}. Please take a moment to pay us today. <br/><br />
                                    // All transactions are securely processed through our payment gateway.`

                                    // ----------First time chage -----------
                                    // __html: invoice_details.length == 1 ?
                                    //     `Hi <b>${userData?.patient_name}</b>, <br/><br />
                                    // You have $${(sum_of_pending_amounts && showAmount) || 0} of unpaid balance for the invoice [${commaSeparatedInvoiceNumber}] to ${userData?.hospital_name} for services provided on ${commaSeperatedDates}. <br/><br />
                                    // All transactions are securely processed through our payment gateway.`
                                    //     : `Hi <b>${userData?.patient_name}</b>, <br/><br />
                                    //     You have $${(sum_of_pending_amounts && showAmount) || 0} of unpaid balance to ${userData?.hospital_name} for services provided on multiple dates.  <br/><br />
                                    //     All transactions are securely processed through our payment gateway.`
                                }}
                            />
                        </Grid>

                        {invoice_details.length > 1 ? <Grid item sx={{
                            borderStyle: "dashed",
                            borderWidth: "2px 0px",
                            borderColor: '#FFFFFF80',
                            // borderColor: "#1B92AC",
                            // padding: sm || md ? '3% 0px' : '13% 0px',
                        }}>
                            <Field
                                name={"amount_details"}
                                label={""}
                                component={TableCheckboxField}
                                columns={invoiceDetailsColumns().columns}
                                records={(invoice_details || []).map(data => Object.assign({}, {
                                    id: data?.invoice_number
                                }, data)) || []}
                            />
                        </Grid> : null}

                        {!sm && !md ? <Grid item
                            sx={{
                                ...((invoice_details && invoice_details.length == 1) ? { flexBasis: '40%', display: 'flex', margin: '0px' } : { marginTop: 'auto' })
                            }}
                        >
                            <Grid
                                // sx={{
                                //     ...((invoice_details && invoice_details.length == 1) ? { justifyContent: 'space-between', display: 'flex', flexDirection: 'column' } : { display: 'inline-block' })
                                // }}
                                //-------Changed for removed below line of button--------
                                sx={{ justifyContent: 'space-between', display: 'flex', flexDirection: 'column', flex: 1 }}
                            >
                                {!sm && !md ?
                                    <Grid item
                                        // xs={12}
                                        sx={{
                                            padding: '0 2em',
                                            marginTop: (invoice_details && invoice_details.length == 1) ? '0px' : sm || md ? '3%' : '10%',
                                            display: 'flex',
                                            alignItems: 'center',
                                            flexDirection: 'column',
                                            width: '100%'
                                            // minWidth: xl ? '400px' : 'max-conent'
                                            // minWidth: '80%',
                                            // width: '80%'
                                            // width: 'calc(100% -300px)'
                                        }}>
                                        <LoadingButton
                                            className={classes.loadingButton}
                                            loading={btnload}
                                            type='submit'
                                            fullWidth
                                            variant='contained'
                                            color='primary'
                                            disabled={sum_of_pending_amounts === 0}
                                        >{`Pay $${payableAmount || 0}`}
                                        </LoadingButton>

                                    </Grid>
                                    : null}

                                {/* {!sm && !md ? <Grid item
                                    // xs={12}
                                    sx={{
                                        marginTop: sm || md ? '3%' : '10%',
                                        alignSelf: 'flex-end'
                                        // display: 'flex',
                                        // alignItems: 'center',
                                        // flexDirection: 'column',
                                    }}>
                                    <Typography variant="subTitle1" component={"div"} sx={{
                                        lineHeight: 1.5,
                                        fontSize: '14px',
                                        fontFamily: 'Poppins-Regular',
                                        textAlign: 'center',
                                        color: '#fff',
                                        // marginTop: sm || md ? '3%' : '13%',
                                    }}>
                                        {`In case you have any questions regarding your bill, please contact at ${hospital_phone_number}.`}
                                    </Typography>
                                </Grid>
                                    : null} */}
                            </Grid>
                        </Grid>
                            : null}
                    </Grid>
                    {sm || md ? fieldsContainer : null}
                </Grid>
            </form>
        </Grid>

    )
})


Form = reduxForm({
    form: 'payment_Form',
    enableReinitialize: true,
    validate,
    touchOnChange: true,
})(Form);

const selector = formValueSelector('payment_Form'); // <-- same as form name
Form = connect((state, props) => {

    // const { fields } = props;
    // can select values individually
    const hasValues = (["amount_details"] || []).reduce((acc, column) => {
        acc[column] = selector(state, column);
        return acc;
    }, {});

    return {
        hasValues,
    }
})(Form)

export default Form;