
import React from "react";
import moment from "moment";

export function paymentForm() {
    return {
        columns: [
            {
                id: 1,
                value: 'name',
                label: 'Cardholder Name',
                placeholder: 'Cardholder Name',
                editRecord: true,
                viewRecord: true,
                viewMode: true,
                visible: true,
                type: 'input',
                mandatory: true
            },
            {
                id: 2,
                value: 'address',
                label: 'Billing Address',
                placeholder: 'Street Address',
                editRecord: true,
                viewRecord: false,
                viewMode: false,
                visible: false,
                type: 'input',
                mandatory: true
            },
            {
                id: 6,
                value: 'postal_code',
                label: 'Postal Code',
                placeholder: '92122',
                editRecord: true,
                viewRecord: false,
                viewMode: false,
                visible: false,
                type: 'pin',
                mandatory: true
            },
            // {
            //     id: 3,
            //     value: 'city',
            //     label: 'City',
            //     placeholder: 'San Diego',
            //     editRecord: true,
            //     viewRecord: false,
            //     viewMode: false,
            //     visible: false,
            //     type: 'input',
            //     mandatory: true
            // },
            {
                id: 4,
                value: 'state',
                label: 'State',
                placeholder: 'CA',
                editRecord: true,
                viewRecord: false,
                viewMode: false,
                visible: false,
                type: 'input',
                mandatory: true
            },
            {
                id: 3,
                value: 'city',
                label: 'City',
                placeholder: 'San Diego',
                editRecord: true,
                viewRecord: false,
                viewMode: false,
                visible: false,
                type: 'input',
                mandatory: true
            },
            // {
            //     id: 5,
            //     value: 'country',
            //     label: 'Country',
            //     placeholder: 'US',
            //     editRecord: true,
            //     viewRecord: false,
            //     viewMode: false,
            //     visible: false,
            //     type: 'input',
            //     mandatory: true
            // },

            {
                id: 7,
                value: 'cardDetails',
                label: 'Card Number',
                editRecord: true,
                viewRecord: false,
                viewMode: false,
                visible: false,
                mandatory: true,
                showRequired: true,
                type: 'stripe'
            },
        ]
    }
}

export function invoiceDetailsColumns() {
    return {
        columns: [
            {
                id: 1,
                value: 'invoice_date',
                label: 'Service Date',
                // width: '70px',
                width: '18%',
                html: (row, rowValue) => row['invoice_date'] && moment(row['invoice_date']).format('MM/DD/YYYY') || ''
            },
            {
                id: 2,
                value: 'invoice_number',
                label: 'Account #',
                // width: '80px',
                width: '18%',
            },
            {
                id: 3,
                value: 'invoice_amount',
                label: 'Amount',
                // width: '70px',
                width: '18%',
                html: (row, rowValue) => {
                    // console.log("rowValue = ", rowValue);
                    let rounded = parseFloat(rowValue || 0).toFixed(2);
                    return row['invoice_amount'] && `$${rounded}` || '$0'
                }
            },
        ]
    }
}