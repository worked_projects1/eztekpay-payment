import React from 'react';
import { Page, Text, View, Document, StyleSheet, Image } from '@react-pdf/renderer';
import { Font } from '@react-pdf/renderer';
import font from "../../fonts/Poppins-Regular_1.ttf";
import fontSemiBold from "../../fonts/Poppins-SemiBold 10.ttf";
import moment from 'moment';
import { AsYouType } from 'libphonenumber-js';

Font.register({
    family: 'Poppins', fonts: [
        {
            src: font,
            fontWeight: 400,
        },
        {
            src: fontSemiBold,
            fontWeight: 600,
        }
    ]
});

const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        fontFamily: 'Poppins',
        padding: 35
    },
    section: {

    },
    table: {
        width: '100%',
        marginTop: 20
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        height: 37
    },
    header: {
        borderTop: '1px solid grey',
    },
    bold: {
        fontWeight: 'bold',
    },
    row1: {
        // width: '30%',
        width: '40%',
        flexGrow: 0,
        fontSize: 9,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderBottom: '1px solid grey',
        borderLeft: '1px solid grey',
        borderCollapse: 'collapse',
        flexWrap: "wrap",
        flexDirection: 'column',
        padding: 2
    },
    row2: {
        // width: '30%',
        width: '40%',
        fontSize: 9,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderBottom: '1px solid grey',
        borderLeft: '1px solid grey',
        borderCollapse: 'collapse',
        padding: 2
    },
    row3: {
        // width: '30%',
        width: '40%',
        fontSize: 9,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderBottom: '1px solid grey',
        borderLeft: '1px solid grey',
        borderCollapse: 'collapse',
        padding: 2
    },
    row4: {
        // width: '30%',
        width: '40%',
        fontSize: 9,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderBottom: '1px solid grey',
        borderLeft: '1px solid grey',
        borderCollapse: 'collapse',
        padding: 2,
        color: '#2DC146'
    },
    row4Header: {
        // width: '30%',
        width: '40%',
        fontSize: 9,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderBottom: '1px solid grey',
        borderLeft: '1px solid grey',
        borderCollapse: 'collapse',
        padding: 2,
    },
    subtotalRow: {
        display: 'flex',
        flexDirection: 'row',
        height: 37,
    },
    shippingTotalRow: {
        display: 'flex',
        flexDirection: 'row',
        height: 37,
    },
    totalRow: {
        display: 'flex',
        flexDirection: 'row',
        height: 37,
    },
    lastCol: {
        borderRight: '1px solid grey',
        borderCollapse: 'collapse',
    },
    lastRow: {
        borderBottom: '1px solid grey'
    },
    row1Noborder: {
        // width: '30%',
        width: '40%',
        fontSize: 10,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        padding: 2
    },
    row2Noborder: {
        // width: '30%',
        width: '40%',
        fontSize: 10,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        padding: 2
    },
    row3Noborder: {
        // width: '30%',
        width: '40%',
        fontSize: 10,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderBottom: '1px solid grey',
        borderLeft: '1px solid grey',
        borderRight: '1px solid grey',
        padding: 2
    },
    imageView: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'flex-end',
        height: '100px'
    },
    image: {
        // width: '100px',
        // height: '100px',
        width: '100%',
        height: 'auto',
        objectFit: 'scale-down'
        // height: '50%'
    },
    cardimage: {
        width: '90%',
        height: 'auto',
        objectFit: 'scale-down'
    },
    border: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: 'red',
    },
    section1: {
        margin: 10,
        padding: 10,
        flexGrow: 1,
    },
    noteDiv: {
        marginTop: 20,
        fontSize: 9
    },
    thanksDiv: {
        marginTop: 30,
        textAlign: 'center',
        width: '100%',
        fontSize: 10
    }
});

const dynamicData = (data) => (data || []).map((row, i) => {
    const showAmount = parseFloat(row.invoice_amount || 0).toFixed(2);
    return (
        <View key={i} style={styles.row}>
            <View style={[styles.row1]}>
                <Text>{row.invoice_number.split('')}</Text>
            </View>
            <View style={[styles.row2]}>
                <Text
                >{moment(row?.invoice_date).format('MM/DD/YYYY')}</Text>
            </View>
            <View style={[styles.row3, styles.lastCol]}>
                <Text
                >${showAmount}</Text>
            </View>
            {/* <View style={[styles.row4, styles.lastCol, styles.bold]}>
                <Text
                >Paid</Text>
            </View> */}
        </View>
    )
});

const PdfCreator = (props) => {
    const { userData, patient_data, amount_details, cardData } = props;

    const { hospital_details } = userData || {};
    const { name: hospital_name, phone_number, address: hospital_address, city: hospital_city, state: hospital_state, zip_code: hospital_Zip_code, logo_file } = hospital_details || {};
    const { card } = cardData?.paymentMethod || {}

    const hospital_phone_number = new AsYouType('US').input(phone_number);

    const sum_of_invoice_amount = (amount_details || []).reduce((sum, item) => {
        return sum + (item?.invoice_amount || 0);
    }, 0);
    // const decimalPlaces = 2;
    // const showAmount = Math.round(sum_of_invoice_amount * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces);
    const showAmount = parseFloat(sum_of_invoice_amount || 0).toFixed(2);
    var countperPage = 0;

    return (
        <Document>
            <Page size="A4" style={styles.page}>
                <View style={styles.section}>
                    <View style={[styles.imageView, {
                        // backgroundColor: '#88c999'
                    }]}>
                        <View style={{
                            textAlign: 'left',
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                            // alignItems: 'flex-start',
                            // justifyContent: 'flex-start',
                            // backgroundColor: '#b1bbc6',
                            width: '20%',
                            // height: ''
                            height: '75%',
                            marginTop: 20
                        }}>
                            <Image src={{ uri: logo_file, method: "GET", headers: { "Cache-Control": "no-cache" }, body: '' }} style={styles.image} alt='image svg url'
                            />
                        </View>
                        <View style={{
                            textAlign: 'right',
                            display: 'flex',
                            width: '30%',
                            // backgroundColor: '#9084a6'
                        }}>
                            <View style={{
                                border: '2px solid #4F81BD',
                                backgroundColor: '#36B050',
                                width: '100%',
                                textAlign: 'center',
                                height: '32',
                                display: 'flex',
                                justifyContent: 'center'
                            }}>
                                <Text style={{
                                    fontSize: 12
                                }}> Paid</Text>

                            </View>
                        </View>
                    </View>
                    <View style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        flexDirection: 'row'
                    }}>
                        <View>

                            <View style={[styles.bold, {
                                marginTop: 10,
                                fontSize: 12
                            }]}><Text >{hospital_name}</Text></View>
                            <View style={{
                                fontSize: 10,
                                marginTop: 5
                            }}><Text>{hospital_address}</Text></View>
                            <View style={{
                                fontSize: 10
                            }}><Text>{hospital_city}</Text></View>
                            <View style={{
                                fontSize: 10
                            }}><Text>{hospital_state} {hospital_Zip_code}</Text></View>
                            <View style={{
                                fontSize: 10
                            }}><Text>{hospital_phone_number}</Text></View>


                            <View style={[styles.bold,
                            {
                                fontSize: 10,
                                marginTop: 10
                            }]}><Text>TO:</Text></View>
                            <View style={{
                                fontSize: 10
                            }}><Text>{patient_data?.name}</Text></View>
                            <View style={{
                                fontSize: 10
                            }}><Text>{patient_data?.address?.line1}</Text></View>
                            <View style={{
                                fontSize: 10
                            }}><Text>{patient_data?.address?.city}</Text></View>
                            <View style={{
                                fontSize: 10
                            }}><Text>{patient_data?.address?.state} {patient_data?.address?.postal_code}</Text></View>
                        </View>
                        <View style={{
                            display: 'flex',
                            // backgroundColor: 'grey',
                            alignItems: 'flex-end'
                        }}>
                            <View style={[styles.bold, {
                                fontSize: 16,
                                marginTop: 10,
                                textAlign: 'right',
                                // backgroundColor: '#c2ebdc78',
                            }]}><Text style={{
                            }}>RECEIPT</Text></View>
                            <View style={{
                                fontSize: 10,
                                marginTop: 10,
                                // backgroundColor: '#ebc2cb78'
                            }}><Text >DATE: {moment().format('MM/DD/YYYY')}</Text></View>
                        </View>
                    </View>

                    {/* //-----------------------------------table --------------------------------*/}

                    <View style={styles.table}>

                        {dynamicData(amount_details).map((item, index) => {

                            if (index == 0) {
                                countperPage = countperPage + 1;
                                return <View key={index} wrap={false}>
                                    <View style={[styles.row, styles.bold, styles.header]} >
                                        <View style={styles.row1}>
                                            <Text >Account #</Text>
                                        </View>
                                        <View style={styles.row2}>
                                            <Text>{(hospital_name.replace(/\s+/g, "") || "").toLowerCase() == "mycaresmartpay" ? "Statement Date" : "Service Date"}</Text>
                                        </View>
                                        <View style={[styles.row3, styles.lastCol]}>
                                            {/* <Text >Account Balance</Text> */}
                                            <Text>Amount Paid</Text>
                                        </View>
                                        {/* <View style={[styles.row4Header, styles.lastCol]}>
                                            <Text
                                            >Status</Text>
                                        </View> */}
                                    </View>
                                    <React.Fragment key={index}>{item}</React.Fragment>
                                </View>
                            } else if ((index == 11 && countperPage == 11) || (countperPage == 18)) {
                                countperPage = 0;
                                return <View key={index} wrap={false} break={true}>
                                    <View style={[styles.row, styles.bold, styles.header]} >
                                        <View style={styles.row1}>
                                            <Text >Account #</Text>
                                        </View>
                                        <View style={styles.row2}>
                                            <Text>{(hospital_name.replace(/\s+/g, "") || "").toLowerCase() == "mycaresmartpay" ? "Statement Date" : "Service Date"}</Text>
                                        </View>
                                        <View style={[styles.row3, styles.lastCol]}>
                                            {/* <Text >Account Balance</Text> */}
                                            <Text>Amount Paid</Text>
                                        </View>
                                        {/* <View style={[styles.row4Header, styles.lastCol]}>
                                            <Text
                                            >Status</Text>
                                        </View> */}
                                    </View>
                                    <React.Fragment key={index}>{item}</React.Fragment>
                                </View>

                            } else {
                                countperPage = countperPage + 1;
                                return <View key={index} wrap={false} break={false}>
                                    <React.Fragment key={index}>{item}</React.Fragment>
                                </View>
                            }

                        })}
                        {amount_details.length > 1 ? <View style={[styles.totalRow]}>
                            <View style={styles.row1Noborder}>
                                <Text ></Text>
                            </View>
                            {/* <View style={styles.row1Noborder}>
                                <Text ></Text>
                            </View> */}
                            <View style={styles.row2Noborder}>
                                <Text >TOTAL</Text>
                            </View>
                            <View style={[styles.row3Noborder]}>
                                <Text >${showAmount}</Text>
                            </View>

                        </View> : null}
                    </View>
                    {/* <View style={styles.noteDiv}>
                        <Text>
                            If you have any questions concerning this invoice, please contact at {hospital_phone_number}
                        </Text>
                    </View> */}

                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        marginTop: 20,
                        // marginLeft: 5
                    }}>
                        <View style={{
                            width: '5%',
                            height: 'auto',
                            objectFit: 'scale-down',
                            marginRight: '5px',
                            // backgroundColor: '#88c999',
                            marginTop: 2
                        }}>
                            <Image src={{ uri: process.env.REACT_APP_PAYMENT_CARD || "", method: "GET", headers: { "Cache-Control": "no-cache" }, body: '' }} style={styles.cardimage} alt='image svg url'
                            />
                        </View>
                        <View>
                            <View>
                                <Text style={[styles.bold,
                                {
                                    fontSize: 10
                                }]}>Payment Method</Text>
                            </View>
                            <View>
                                <Text style={{
                                    textTransform: 'capitalize',
                                    fontSize: 10
                                }}>
                                    {`${(card?.display_brand || "").replace("_", " ")} x${card?.last4 || ""}`}
                                </Text>
                            </View>

                            {/* <View>
                                <Text style={{
                                    textTransform: 'capitalize',
                                    fontSize: 10
                                }}>{(card?.display_brand || "").replace("_", " ")}</Text>
                            </View>
                            <View style={{
                                fontSize: 10,
                            }}>
                                <Text>{`xxxx xxxx xxxx ${card?.last4 || ""}`}</Text>
                            </View> */}
                        </View>
                    </View>
                    <View style={[styles.thanksDiv, styles.bold]}>
                        <Text>
                            Thank you for your payment
                        </Text>
                    </View>
                </View>
            </Page>
        </Document>
    )
};

export default PdfCreator;

