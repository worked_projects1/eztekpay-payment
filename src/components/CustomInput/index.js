/**
 * 
 * Custom Input Field
 * 
 */

import React from 'react';
import { InputLabel, TextField, Grid, Typography, InputAdornment } from '@mui/material';
import useStyles from './styles';

export default function CustomInput(props) {
    const { input, label, placeholder, disabled, defaultBlur, inputStyle, labelClass, customLabel, adornmentStyle, meta: { touched, error, warning } } = props;

    const { classes } = useStyles();

    const { name, value, onChange } = input;

    const InputChange = defaultBlur ? Object.assign({}, {
        onBlur: (e) => onChange(e.target.value),
        defaultValue: value || ''
    }) : Object.assign({}, {
        onChange: (e) => onChange(e.target.value),
        value: value || ''
    });

    const inputAdormentProps = {
        ...(props?.startAdornment && { startAdornment: <InputAdornment position="start" sx={adornmentStyle || {}}>{props?.startAdornment}</InputAdornment> }),
    };

    return (
        <Grid container sx={{
            flexDirection: 'column'
        }}>
            <Grid item xs={12} className={classes.formControl}>
                <InputLabel
                    shrink={false}
                    htmlFor={name}
                    className={labelClass}
                >
                    <Typography className={customLabel || classes.label}>{label}&nbsp;{props?.helperText ? touched && ((error && <span className={classes.error}>{error}</span>) || (warning && <span>{warning}</span>)) : null}</Typography>

                </InputLabel>
                <Grid
                    sx={{
                        padding: '3px',

                    }}
                >
                    <TextField
                        // fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                        type={props.type}
                        InputProps={{
                            classes: { input: classes.inputs },
                            className: classes.amountField,
                            placeholder: placeholder,
                            ...inputAdormentProps
                        }}
                        name={name}
                        className={classes.textField}
                        // defaultValue={input?.value || ''}
                        {...InputChange}
                        disabled={disabled}
                        // onChange={(e) => input?.onChange(e.target.value)}
                        sx={{
                            width: '100%',
                            '& .MuiOutlinedInput-root': {
                                borderRadius: '7px',
                                height: 50,
                                border: '1px solid #E7E7E7',
                                boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
                                ':hover': {
                                    border: '0.5px solid #E7E7E7 !important',
                                    // boxShadow: '-1px 1px 4px 4px #E7E7E7'
                                },
                                ':focus-within': { border: '0.5px solid #E7E7E7 !important' }
                            },
                            '& .MuiOutlinedInput-root.Mui-disabled': {
                                opacity: 0.5,
                                '& .MuiTypography-root': {
                                    opacity: 0.5,
                                },
                                ':hover': {
                                    border: '1px solid #E7E7E7 !important',
                                    boxShadow: 'none'
                                }
                            },
                            '& .MuiOutlinedInput-notchedOutline': {
                                border: 'none'
                            },
                            '& p': {
                                textAlign: 'center'
                            },
                            '& input': inputStyle || {}
                        }}
                        variant="outlined"
                        helperText={props?.helperText}
                    />
                </Grid>
            </Grid>
            {props?.helperText ? null : <Grid className={classes.errorContainer}>
                {touched && ((error && <span className={classes.error}>{error}</span>) || (warning && <span>{warning}</span>))}
            </Grid>}
        </Grid>
    )

}