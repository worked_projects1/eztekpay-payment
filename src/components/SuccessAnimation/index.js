import React from 'react';
import './styles.css';
import Stars from '../../images/Stars.svg';

const SuccessAnimation = () => {
    return <div class="main-container">
        <div style={{ alignSelf: 'flex-end' }} class="stars1">
            <img src={Stars} alt='Stars svg' />
        </div>
        <div class="check-container">

            <div class="check-background">
                <svg viewBox="0 0 65 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7 25L27.3077 44L58.5 7" stroke="white" stroke-width="13" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>

            <div class="check-shadow"></div>
        </div>
        <div style={{ alignSelf: 'flex-start' }} class="stars2">
            <img src={Stars} alt='Stars svg' />
        </div>
    </div>
}
export default SuccessAnimation;