
/***
 *
 * Stripe Field
 *
 */


import React, { forwardRef, useImperativeHandle, useRef } from 'react';
import useStyles from './styles';
import { CardElement } from '@stripe/react-stripe-js';
import { TextField, Grid, Typography, InputLabel, useMediaQuery } from '@mui/material';
import { useTheme } from "@mui/system";
import Icons from '../Icons';

const CardComponent = forwardRef((props, ref) => {
  const { inputRef, onChange, error, warning, setErrorMessage, ...other } = props;
  const { classes } = useStyles();
  const elementRef = useRef();
  useImperativeHandle(inputRef, () => ({
    focus: () => elementRef.current.focus
  }));

  const CARD_OPTIONS = {
    iconStyle: 'solid',
    style: {
      base: {
        // iconColor: '#2DCFC1',
        iconColor: '#385C9F',
        color: "#344055",
        fontWeight: 400,
        // fontFamily: 'Poppins-Regular',
        fontSize: '14px',
        fontSmoothing: 'antialiased',
        ':-webkit-autofill': {
          color: "#344055",
        },
        '::placeholder': {
          fontWeight: 400,
          color: '#CBDAD7',
        },
      },
      invalid: {
        iconColor: '#F20056',
        color: 'black',
      },
    },
    hidePostalCode: true,
    disableLink: true
  };

  return (
    <div className={classes.stripeField}>
      <CardElement
        options={CARD_OPTIONS}
        className={classes.cardCustom}
        disableLink={true}
        ref={ref}
        onChange={(e) => {
          setErrorMessage(e.error && e.error.message);
          onChange({ target: { value: e } })
        }}
        onReady={element => (elementRef.current = element)}
      />
    </div>

  )
});

CardComponent.displayName = 'CardComponent';

export default function StripeField(props) {
  const { input, meta: { touched, error, warning } } = props;

  const { classes } = useStyles();
  const theme = useTheme();
  const [errorMessage, setErrorMessage] = React.useState(false);
  const lg = useMediaQuery(theme.breakpoints.down('xl'));
  const { name, value, onChange } = input;
  // 'card1', 'union_pay' 
  const cards = ['visa', 'masterCard', 'amex', 'discover'];

  return (
    <Grid container sx={{
      flexDirection: 'column'
    }}>
      <Grid item xs={12} className={classes.formControl}>
        <InputLabel
          shrink={false}
          htmlFor={name}
        >
          <Typography className={classes.label}>{props?.label}</Typography>
        </InputLabel>
        <Grid
          sx={{
            padding: '3px'
          }}
        >
          <TextField
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            type={props.type}
            name={name}
            onChange={(e) => {
              onChange(e.target.value)
            }}
            value={(value && value) || ''}
            // value={value && phoneType.input(value) || ''}
            // defaultValue={field?.value || ''}
            InputProps={{
              classes: { input: classes.inputs },
              placeholder: props.placeholder,
              inputComponent: CardComponent,
              name: name,

            }}
            inputProps={{
              setErrorMessage: setErrorMessage
            }}
            className={classes.textField}
            sx={{
              '& .MuiOutlinedInput-root': {
                borderRadius: "50px",
                border: "1px solid #E7E7E7 !important",
                background: "#FFF",
                boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
                padding: theme.spacing(0, 3),
                color: "#344055",
                fontSize: '14px',
                ':hover': {
                  border: '0.5px solid #E7E7E7 !important',
                },
                ':focus-within': { border: '0.5px solid #E7E7E7 !important' }
              },
              '& .MuiOutlinedInput-root.Mui-disabled': {
                ':hover': {
                  border: '1px solid #E7E7E7 !important',
                  boxShadow: 'none'
                }
              },
              '& .MuiOutlinedInput-notchedOutline': {
                border: 'none'
              }
            }}
            variant="outlined"
          />
        </Grid>
      </Grid>
      <Grid className={classes.errorContainer}>
        {((touched && (error && <span className={classes.error}>{error}</span>))
          || (warning && <span className={classes.error}>{warning}</span>)) || <span className={classes.error}>{errorMessage}</span>}
      </Grid>

      <div
        style={{
          marginTop: '5px',
          display: "flex",
          flexFlow: "row",
          alignItems: "flex-start",
          justifyContent: 'center',
          padding: '0px 10px'
          // objectFit: 'scale-down'
        }}>
        {cards?.map((e, i) => <Icons type={e} key={i}
          style={{
            // marginRight: cards.length == i + 1 ? '0px' : '5px',
            marginRight: '5px'
            // width: lg ? '15%' : '50%',
            // width: lg ? '10%' : '15%',
          }}
          className={classes.cards}
        />
        )}
        <Icons type={'stripe'}
          className={classes.stripeBy}
          style={{
            // width: lg ? '60%' : '100%'
            // width: lg ? '15%' : '22%'
          }} />
      </div>

    </Grid >
  )
}