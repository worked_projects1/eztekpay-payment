

import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({

  stripeField: {
    height: 50,
    // padding: '12px 0px',
    padding: theme.spacing(2, 0),
    width: '100%'
  },
  cardCustom: {
    fontFamily: 'Poppins-Regular !important',
  },
  label: {
    fontSize: '15px',
    color: '#344055',
    marginLeft: '18px',
    fontWeight: 'normal',
  },
  textField: {
    '& .MuiOutlinedInput-root': {
      borderRadius: "50px",
      border: "1px solid #E7E7E7 !important",
      background: "#FFF",
      boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.05)",
    },
    '& :after': {
      border: "1px solid #E7E7E7 !important",
    },
    '& :before': {
      border: "1px solid #E7E7E7 !important",
    },
    input: {
      padding: theme.spacing(1.5, 3),
      color: "#344055",
      fontSize: '14px'
    }
  },
  inputs: {
    "&::placeholder": {
      fontWeight: 400,
      color: '#CBDAD7',
      opacity: "1 !important",
    },
  },
  error: {
    fontSize: '12px',
    color: 'red',
    marginLeft: '18px',
    lineHeight: '15px',
    display: 'block'
  },
  phoneInput: {
    width: '100%',
    border: 'none !important',
    outline: 'none !important',
    boxShadow: 'none !important',
    borderRadius: "50px",
    "&: focus-visible": {
      border: 'none !important',
      outline: 'none !important',
      boxShadow: 'none !important'
    }
  },
  errorContainer: {
    height: '1em',
    // marginBottom: '0.5em',
  },
  cards: {
    // width: '14%',
    // [theme.breakpoints.up('xl')]: {
    //   // width: '19%',
    // },
    // height: "50%",
    // marginLeft: 'auto'
    // marginRight: '5px',
    // flex: "1 0 auto",
    // maxWidth: "100%",
    // objectFit: "contain"
    // maxWidth: "45px"
    [theme.breakpoints.up('xl')]: {
      maxHeight: '50px',
      // marginLeft: 'auto'
    },
    // [theme.breakpoints.up('md')]: {
    //   maxHeight: '30px',
    //   // marginLeft: 'auto'
    // },
    [theme.breakpoints.down('sm')]: {
      maxHeight: '28px  !important',
      // marginLeft: 'auto'
    },
    // [theme.breakpoints.up('xs')]: {
    //   maxHeight: '30px',
    //   // marginLeft: 'auto'
    // },
    // maxHeight: '35px',
    "@media (min-width: 1024px)": {
      maxHeight: '30px',
    },
    "@media (max-width: 1024px)": {
      maxHeight: '30px',
    },
    "@media (max-width: 600px)": {
      maxHeight: '35px',
    },
    "@media (max-width: 320px)": {
      maxHeight: '24px !important',
    },
    "@media (max-width: 280px)": {
      maxHeight: '20px !important',
    },
  },
  stripeBy: {
    // width: '21%',
    // height: "50%",
    // [theme.breakpoints.up('xl')]: {
    //   // width: '27%',
    // },
    // maxWidth: '70px'
    [theme.breakpoints.up('xl')]: {
      maxHeight: '50px',
      marginLeft: 'auto'
    },
    // [theme.breakpoints.up('md')]: {
    //   maxHeight: '30px',
    //   marginLeft: 'auto'
    // },
    "@media (min-width: 1024px)": {
      maxHeight: '30px',
      marginLeft: 'auto'
    },
    [theme.breakpoints.down('sm')]: {
      maxHeight: '28px !important',
      marginLeft: 'auto'
    },
    // [theme.breakpoints.up('xs')]: {
    //   maxHeight: '30px',
    //   marginLeft: 'auto'
    // },
    // maxHeight: '35px',
    // marginLeft: 'auto'
    "@media (max-width: 1024px)": {
      maxHeight: '30px',
      marginLeft: 'auto'
    },
    "@media (max-width: 600px)": {
      maxHeight: '35px',
      marginLeft: 'auto'
    },
    "@media (max-width: 320px)": {
      maxHeight: '24px !important',
      marginLeft: 'auto'
    },
    "@media (max-width: 280px)": {
      maxHeight: '20px !important',
      marginLeft: 'auto'
    },
    // "@media (min-width: 600px)": {
    //   maxHeight: '35px',
    //   marginLeft: 'auto'
    // }
  },
  imgDiv: {
    // display: 'flex',
    // flexFlow: 'row',
    marginRight: '5px',
    // objectFit: "contain",
    maxHeight: '20px'
    // alignItems: "flex-start",
    // flex: 1
  }
}));


export default useStyles;