
/**
 * 
 * Spinner 
 * 
 */

import React from 'react';
import ScaleLoader from "react-spinners/ScaleLoader";
import { Grid } from '@mui/material';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function Spinner(props) {

    return (<Grid container style={props.style} className={props.className}>
        {props && props.showHeight && <Grid item xs={12} style={{ height: '280px' }} /> || ''}
        <Grid item xs={12} style={{ textAlign: 'center' }}>
            <ScaleLoader
                size={100}
                height={25}
                // color={"#2DCFC1"}
                color="#385C9F"
                loading={true}
            />
        </Grid>
    </Grid>)
}