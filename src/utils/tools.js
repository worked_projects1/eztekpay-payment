import moment from "moment";

function checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
        var num = parseInt(str);
        if (isNaN(num) || num <= 0 || num > max) num = 1;
        str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
    };
    return str;
};

export function normalize(field) {
    return (val) => {

        if (field.value === 'postal_code') {
            let value = val.replace(/\D/g, '');
            return value;
        }
        else if (field.value == 'otp_secret') {
            // console.log("val = ", val);
            // let cleanedValue = val.replace(/[^0-9/]/g, '');
            // let formattedDate = val.replace(/(\d{2})(\d{2})(\d{2})/, '$1/$2/$3');
            let formattedDate = val;
            // this.type = 'text';
            var input = formattedDate;
            if (/\D\/$/.test(input)) {
                // console.log("entered here");
                // input = input.substr(0, input.length - 2);
                input = input.substr(0, input.length - 3);
                // console.log("input = ", input);
            }
            var values = input.split('/').map(function (v) {
                return v.replace(/\D/g, '')
            });
            if (values[0]) values[0] = checkValue(values[0], 12);
            if (values[1]) values[1] = checkValue(values[1], 31);
            var output = values.map(function (v, i) {
                return v.length == 2 && i < 2 ? v + ' / ' : v;
            });
            let finalOutput = output.join('').substr(0, 14);
            // if (formattedDate.length == 2 && formattedDate[2] != '/') {
            //     formattedDate = formattedDate.substring(0, 2) + '/' + formattedDate.substring(2, formattedDate.length - 1)
            // }
            // if (formattedDate.length == 6 && formattedDate[5] != '/') {
            //     formattedDate = formattedDate.substring(0, 5) + '/' + formattedDate.substring(5, formattedDate.length - 1)
            // }

            // formattedDate = moment(formattedDate).format("MM/DD/YYYY");
            // console.log("formattedDate = ", moment(formattedDate).format("MM/DD/YYYY"));

            // console.log("finalOutput = ", finalOutput);
            return finalOutput;
        }
        else {
            return val
        }
    }
}