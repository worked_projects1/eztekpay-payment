import axios from 'axios';

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL || 'http://localhost:3000/test/',
    timeout: 40000,
    headers: { Accept: 'application/json' },
});

export function setAuthToken(authToken) {
    api.defaults.headers.common['Authorization'] = authToken;
}

export const staticApi = axios.create({
    baseURL: process.env.REACT_APP_ZIPCODES_URL,
    timeout: 40000,
    headers: { Accept: 'application/json' },
});

export function uploadFile(uploadUrl, data, contentType) {
    return axios.put(uploadUrl, data, {
        headers: {
            Accept: 'application/json',
            'Content-Type': contentType,
        },
    }).then((response) => response.data);
}


export default api;