
export const rootPath = '/';

const routes = {
    HOME: `${rootPath}`,
    OTP_FORM: `${rootPath}:id?`,
    OLD_OTP_FORM: `${rootPath}payment_link/:id?`,
    PAYMENT: `${rootPath}payment`,
    SUCCESS: `${rootPath}success`
};

export default routes;